﻿using System;
using System.Xml.Serialization;

namespace _7DTDManager
{


    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    [System.Xml.Serialization.XmlRootAttribute(Namespace = "", IsNullable = false)]
    public class Homes
    {

        private static Homes instance;
        public static Homes Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new Homes();
                }
                return instance;
            }
            set
            {
                instance = value;
            }
        }


        private HomesProfile[] profilesField;

        /// <remarks/>
        [System.Xml.Serialization.XmlArrayItemAttribute("Profile", IsNullable = false)]
        public HomesProfile[] Profiles
        {
            get
            {
                return this.profilesField;
            }
            set
            {
                this.profilesField = value;
            }
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class HomesProfile
    {

        private HomesProfileHome[] homeField;

        private string playerNameField;

        private string steamIDField;

        private int maxHomesField;

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("Home")]
        public HomesProfileHome[] Home
        {
            get
            {
                return this.homeField;
            }
            set
            {
                this.homeField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string PlayerName
        {
            get
            {
                return this.playerNameField;
            }
            set
            {
                this.playerNameField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string SteamID
        {
            get
            {
                return this.steamIDField;
            }
            set
            {
                this.steamIDField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public int maxHomes
        {
            get
            {
                return this.maxHomesField;
            }
            set
            {
                this.maxHomesField = value;
            }
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    public partial class HomesProfileHome
    {

        private string nameField;

        private int xField;

        private int yField;

        private int zField;

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string name
        {
            get
            {
                return this.nameField;
            }
            set
            {
                this.nameField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public int x
        {
            get
            {
                return this.xField;
            }
            set
            {
                this.xField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public int y
        {
            get
            {
                return this.yField;
            }
            set
            {
                this.yField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public int z
        {
            get
            {
                return this.zField;
            }
            set
            {
                this.zField = value;
            }
        }
    }



}

