﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace _7DTDManager
{
     using bb = _bbcode;
    public static class _Kz_Settings
    {
        

        public static string KzVersion = "(v1.33)";
        //public static string servername = "ᴷᶻ[AA00AA]»[FFFFFF]";
        //public static string servernameAll = "ᴷᶻ[7F7FFF]»[FFFFFF]";

        public static string[] versionInfo = {
bb._txt2C + "v.90: /who now shows distance" + bb._cc,
bb._txt2C + "v.92: Added /tpaccept and /tpdeny, /tp <name|id> now allowed for all players" + bb._cc,
bb._txt2C + "v.93: /who now shows compass direction" + bb._cc,
bb._txt2C + "v.94: /info also shows compass direction now" + bb._cc,
bb._txt2C + "v.95: login and updates now sent directly instead of the chathook" + bb._cc,
bb._txt2C + "v.96: GeoIP country code lookup added to login" + bb._cc,
bb._txt2C + "v.97: /motd added, to display all past updates" + bb._cc,
bb._txt2C + "v.98: /gimme added, its not very clever" + bb._cc,
bb._txt2C + "v.99: Internal improvements, custom setting files, file reloads etc" + bb._cc,
bb._txt2C + "v1.00: /gimme improved, listhomes now shows maxhomes" + bb._cc,
bb._txt2C + "v1.01: /idlist now shows country code" + bb._cc,
bb._txt2C + "v1.02: /gimmie zombies -does what it says :D" + bb._cc,
bb._txt2C + "v1.05: Works on A10.1...without chathook, and color codes gone.. sucks" + bb._cc,
bb._txt2C + "v1.06: Text commands back sort of (the slower ones) +admin can issue commands for a player :)" + bb._cc,
bb._txt2C + "v1.08: /who -list ordered by distance" + bb._cc,
bb._txt2C + "v1.11: Working for A10.4, still no color codes.. ask tfp to bring them back!" + bb._cc,
bb._txt2C + "v1.12: /info rg -shows region filename" + bb._cc,
bb._txt2C + "v1.13: Update to A11" + bb._cc,
bb._txt2C + "v1.14: Fixed teleport issues for A11" + bb._cc,
bb._txt2C + "v1.16: Update to A11.2" + bb._cc,
bb._txt2C + "v1.17: Chathook working again for to A11.2" + bb._cc,
bb._txt2C + "v1.2: Most C# commands integrated on 7DTDmanager now" + bb._cc,
bb._txt2C + "v1.21: /kstats [<playername|id>] added [] is optional" + bb._cc,
bb._txt2C + "v1.24: /kstats now shows total votes" + bb._cc,
bb._txt2C + "v1.25: /rankup now gives you extra home if you have more than 10votes" + bb._cc,
bb._txt2C + "v1.26: /gimme back for admins" + bb._cc,
bb._txt2C + "v1.27: /version all for complete changelog" + bb._cc,
bb._txt2C + "v1.28: onJoin support added welcome messages" + bb._cc,
bb._txt2C + "v1.29: Extra config settings, A11.5" + bb._cc,
bb._txt2C + "v1.30: telnetDelay added to config, A11.5" + bb._cc,
bb._txt2C + "v1.31: Start of new warp plugin, A12 update" + bb._cc,
bb._txt2C + "v1.32: Teleport delays removed, /who threaded" + bb._cc,
bb._txt2C + "v1.33: /warp ...has been enhanced" + bb._cc,
bb._txt2C + "-----------------------------------------------"+ bb._cc};
        
        public static string servername = "[FFFFFF]ᴷᶻ[-]";
        public static string servernameAll = "[FFFFFF]ᴷᶻ[-][7F7FFF]»[-]";
        public static bool defaultSay = true;
        public static int telnetDelay = 50; //1000 = 1second

        public static string regionFolder = "c:\\pathTo\\RegionsFolder\\ --UNIX PATH WORKS ASWEl-- /home/7dtd/regions/";
        public static string backupFolder = "d:\\pathTo\\backFolder\\ --UNIX PATH WORKS ASWEL-- /home/backups/";

        public static string fsC = "[2AFF2A]";

        //forwardslash
        public static string cmdC = "[7FFF55]";
        public static string argC = "[7FFF55]";
        public static string txtC = "[FFFFFF]";
        public static string txt2C = "[AABCCF]";
        public static string linkC = "[FF5500]";
        public static string alertC = "[FF5500]";
        public static string warningsC = "[F99A3B]";
        public static string errorsC = "[F50243]";
        public static string cordsC = "[FF8000]";


        public static string bbclose = "[-]";
        public static bool loadedSettings = false;
        public static bool gimme_droppedItem = false;

        public static void loadXML()
        {
            if (System.IO.File.Exists("kzSettings" + ".xml")) {
                try {
                    XmlSerializer deserializer = new XmlSerializer(typeof(Settings));
                    TextReader textReader = new StreamReader("kzSettings" + ".xml");
                    Settings.Instance = (Settings)deserializer.Deserialize(textReader);
                    loadedSettings = true;
                    textReader.Close();
                    //return true;
                } catch (Exception e) {
                    Console.WriteLine("Error in kz_Settings.loadXML: " + e);
                      try {
                          Settings.Instance = new Settings();

                          Settings config = new Settings();
                              config.ConfigVersion = "2";
                          Server sdefault = new Server();
                              sdefault.ServerName = "-.-";
                              sdefault.ServerPrivateChat = "[FFFFFF]ᴷᶻ[-][AA00AA]»[-]";
                              sdefault.ServerPublicChat = "[FFFFFF]ᴷᶻ[-][7F7FFF]»[-]";
                              sdefault.defaultSay = true;
                              sdefault.telnetDelay = 100;
                              sdefault.regionFolder = "c:\\pathTo\\RegionsFolder\\ --UNIX PATH WORKS ASWEl-- /home/7dtd/regions/";
                              sdefault.backupFolder = "d:\\pathTo\\backFolder\\ --UNIX PATH WORKS ASWEL-- /home/backups/";

                            _bbcode bbdefault = new _bbcode();

                          Colors cdefault = new Colors();
                          cdefault.ForwardSlash = "[2AFF2A]";
                          cdefault.Commands = "[7FFF55]";
                          cdefault.Arguements = "[7FFF55]";
                          cdefault.Text = "[FFFFFF]";
                          cdefault.Links = "[FF5500]";
                          cdefault.Warnings = "[F99A3B]";
                          cdefault.Errors = "[F50243]";
                          cdefault.Cords = "[FF8000]";

                          Settings.Instance = config;
                          Settings.Instance.Server = sdefault;
                          Settings.Instance.Colors = cdefault;
                          Settings.Instance.BBCode = bbdefault;
                         loadedSettings = true;
                      } catch (Exception ee) {
                          Console.WriteLine("1Error in kzSettings.saveXML: " + ee);
                      }
                      //return true;
                }
            } else {
                try {
                    Settings.Instance = new Settings();

                    Settings config = new Settings();
                    config.ConfigVersion = "2";
                    Server sdefault = new Server();
                    sdefault.ServerName = "-.-";
                    sdefault.ServerPrivateChat = "[FFFFFF]ᴷᶻ[-][AA00AA]»[-]";
                    sdefault.ServerPublicChat = "[FFFFFF]ᴷᶻ[-][7F7FFF]»[-]";
                    sdefault.defaultSay = true;
                    sdefault.telnetDelay = 100;
                    sdefault.regionFolder = "c:\\pathTo\\RegionsFolder --UNIX PATH WORKS ASWEL-- /home/7dtd/regions";
                    sdefault.backupFolder = "d:\\pathTo\\backFolder --UNIX PATH WORKS ASWEL-- /home/backups";

                    _bbcode bbdefault = new _bbcode();

                    Colors cdefault = new Colors();
                    cdefault.ForwardSlash = "[2AFF2A]";
                    cdefault.Commands = "[7FFF55]";
                    cdefault.Arguements = "[7FFF55]";
                    cdefault.Text = "[FFFFFF]";
                    cdefault.Links = "[FF5500]";
                    cdefault.Warnings = "[F99A3B]";
                    cdefault.Errors = "[F50243]";
                    cdefault.Cords = "[FF8000]";

                    Settings.Instance = config;
                    Settings.Instance.Server = sdefault;
                    Settings.Instance.Colors = cdefault;
                    Settings.Instance.BBCode = bbdefault;
                    loadedSettings = true;
                } catch (Exception e) {
                    Console.WriteLine("Error in kz_Settings.saveXML: " + e);
                }
                try {
                    XmlSerializer serializer = new XmlSerializer(typeof(Settings));
                    TextWriter textWriter = new StreamWriter("kzSettings" + ".xml");
                    serializer.Serialize(textWriter, Settings.Instance);
                    textWriter.Close();
                } catch (Exception e) {
                    Console.WriteLine("Error in kz_Settings.saveXML: " + e);
                }

                //TODO: Add kzSettings.xml autogenerate defaults
                Console.WriteLine("Error in kz_Settings.loadXML: File Not Found");
                //Homes.Instance = new Homes();
                //return true;
            }

        }
        public static void saveXML(string configName = "kzSettings.xml") {
            if (System.IO.File.Exists("kzSettings" + ".xml")) {
                try {
                
                    //Directory.CreateDirectory(GamePrefs.GetString(EnumGamePrefs.SaveGameFolder) + "/playerHomes");
                    XmlSerializer serializer = new XmlSerializer(typeof(Settings));
                    TextWriter textWriter = new StreamWriter(configName);
                    serializer.Serialize(textWriter, Settings.Instance);
                    textWriter.Close();
                } catch (Exception e) {

                    // Log.Out("Error in Sethome.saveXML: " + e);
                }
            }
        }
        public class Settings
        {
            private static Settings instance;
            public static Settings Instance
            {
                get
                {
                    if (instance == null) {
                        instance = new Settings();
                    }
                    return instance;
                }
                set
                {
                    instance = value;
                }
            }
            [XmlAttribute("ConfigVersion")]
            public string ConfigVersion
            { get; set; }

            public Server Server;
            public Colors Colors;

            public _bbcode BBCode; //in seperate file _bbcode.cs
        }

        public class Server {
            //[XmlElement("Name")]
            [XmlAttribute("Name")]
            public string ServerName { get; set; }
            [XmlAttribute("PublicChat")]
            public string ServerPublicChat { get; set; }
            [XmlAttribute("PrivateChat")]
            public string ServerPrivateChat { get; set; }
            [XmlAttribute("defaultSay")]
            public bool defaultSay { get; set; }

            [XmlAttribute("telnetDelay")]
            public int telnetDelay { get; set; }

            //[XmlAttribute("regionFolder")]
            public string regionFolder { get; set; }

            //[XmlAttribute("backupFolder")]
            public string backupFolder { get; set; }
        }

        public class Colors
        {

            [XmlAttribute("FSlash")]
            public string ForwardSlash
            { get; set; }

            [XmlAttribute("Cmds")]
            public string Commands
            { get; set; }

            [XmlAttribute("Args")]
            public string Arguements
            { get; set; }

            [XmlAttribute("Text")]
            public string Text
            { get; set; }

            [XmlAttribute("Links")]
            public string Links
            { get; set; }

            [XmlAttribute("Errors")]
            public string Errors
            { get; set; }

            [XmlAttribute("Warnings")]
            public string Warnings
            { get; set; }

            [XmlAttribute("Cords")]
            public string Cords
            { get; set; }
        }
        /// <summary>
        /// Longest Common Subsequence. A good value is greater than 0.33.
        /// </summary>
        /// <param name="input"></param>
        /// <param name="comparedTo"></param>
        /// <param name="caseSensitive"></param>
        /// <returns>Returns a Tuple of the sub sequence string and the match coeficient.</returns>
        public static Tuple<string, double> LongestCommonSubsequence(this string input, string comparedTo, bool caseSensitive = false) {
            if (string.IsNullOrWhiteSpace(input) || string.IsNullOrWhiteSpace(comparedTo)) return new Tuple<string, double>(string.Empty, 0.0d);
            if (!caseSensitive) {
                input = input.ToLower();
                comparedTo = comparedTo.ToLower();
            }

            int inputLen = input.Length;
            int comparedToLen = comparedTo.Length;

            int[,] lcs = new int[inputLen + 1, comparedToLen + 1];
            LcsDirection[,] tracks = new LcsDirection[inputLen + 1, comparedToLen + 1];
            int[,] w = new int[inputLen + 1, comparedToLen + 1];
            int i, j;

            for (i = 0; i <= inputLen; ++i) {
                lcs[i, 0] = 0;
                tracks[i, 0] = LcsDirection.North;

            }
            for (j = 0; j <= comparedToLen; ++j) {
                lcs[0, j] = 0;
                tracks[0, j] = LcsDirection.West;
            }

            for (i = 1; i <= inputLen; ++i) {
                for (j = 1; j <= comparedToLen; ++j) {
                    if (input[i - 1].Equals(comparedTo[j - 1])) {
                        int k = w[i - 1, j - 1];
                        //lcs[i,j] = lcs[i-1,j-1] + 1;
                        lcs[i, j] = lcs[i - 1, j - 1] + Square(k + 1) - Square(k);
                        tracks[i, j] = LcsDirection.NorthWest;
                        w[i, j] = k + 1;
                    } else {
                        lcs[i, j] = lcs[i - 1, j - 1];
                        tracks[i, j] = LcsDirection.None;
                    }

                    if (lcs[i - 1, j] >= lcs[i, j]) {
                        lcs[i, j] = lcs[i - 1, j];
                        tracks[i, j] = LcsDirection.North;
                        w[i, j] = 0;
                    }

                    if (lcs[i, j - 1] >= lcs[i, j]) {
                        lcs[i, j] = lcs[i, j - 1];
                        tracks[i, j] = LcsDirection.West;
                        w[i, j] = 0;
                    }
                }
            }

            i = inputLen;
            j = comparedToLen;

            string subseq = "";
            double p = lcs[i, j];

            //trace the backtracking matrix.
            while (i > 0 || j > 0) {
                if (tracks[i, j] == LcsDirection.NorthWest) {
                    i--;
                    j--;
                    subseq = input[i] + subseq;
                    //Trace.WriteLine(i + " " + input1[i] + " " + j);
                } else if (tracks[i, j] == LcsDirection.North) {
                    i--;
                } else if (tracks[i, j] == LcsDirection.West) {
                    j--;
                }
            }

            double coef = p / (inputLen * comparedToLen);

            Tuple<string, double> retval = new Tuple<string, double>(subseq, coef);
            return retval;
        }

        private static int Square(int p) {
            return p * p;
        }

        public static void WriteColorLine(string value, ConsoleColor textColor) {
            //
            // This method writes an entire line to the console with the string.
            //
            Console.BackgroundColor = ConsoleColor.Black;
            Console.ForegroundColor = textColor;
            Console.WriteLine(value.PadRight(Console.WindowWidth - 1)); // <-- see note
            //
            // Reset the color.
            //
            Console.ResetColor();
        }
    }


    internal enum LcsDirection {
        None,
        North,
        West,
        NorthWest
    }
}
