﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Text.RegularExpressions;
using System.Xml.Serialization;
using _7DTDManager.Interfaces;
using _7DTDManager.Interfaces.Commands;

namespace _7DTDManager.Commands
{
	using bb = _bbcode;
    public class kz_cmdListHomes : PublicCommandBase
    {
        public kz_cmdListHomes()
        {
            CommandCost = 5;
            //CommandCoolDown = 0;
            CommandHelp = "If you have /sethome <homename>, this command will list them all +extra info";
            CommandName = "listhomes";
        }
        public static string _homeName = "";

        public override bool Execute(IServerConnection server, IPlayer p, params string[] args)
        {
            if (args.Length == 2 && args[1] == "?") {
                cmdHelp.QuestionHelp(CommandName, p);
                return false;
            }
            string message = "";
            try
            {
               
                //set default home
                int numFoundHomes = 1;

                if (args.Length == 1)
                {
                    // m_Console.SendResult("------Listed homes for "+playerName+"------");
                    string[] listhomes = LPlayerHomes(p.SteamID, p.Name, out numFoundHomes);
					message = "------" + bb._txt2C + "Listed homes for " + bb._cc + bb._txtC + p.Name + bb._cc + "[FFFFFF]- " + bb._txt2C + "(Max homes: " + bb._cc + bb._txtC + numFoundHomes + bb._txt2C + ")[-][FFFFFF]-----" + bb._cc;
                    p.Message(message);
                    //m_Console.SendResult(message);
                    foreach (string print in listhomes)
                    {
                        //m_Console.SendResult(print);
                        p.Message(print);
                    }
                    return true;
                }
                try
                {

                    
                    bool listAnotherPlayer = false;

                    if (p.SteamID == "")
                    {
                        listAnotherPlayer = true;
                    }
                    if (listAnotherPlayer)
                    {
                        /*
                        ClientInfo remoteCi = CommonMappingFunctions.GetClientInfoFromNameOrID(_params[0], false);

                        if (remoteCi == null)
                        {
                            //m_Console.SendResult ("Playername or entity id not found.");
                            p.Message(alertC + "Invalid playername: " + txtC + _params[0], servername, _sender);
                            return true;
                        }
                        string remoteSteamid = remoteCi.networkPlayer.guid;
                        string remoteplayerName = remoteCi.playerName;
                        //  m_Console.SendResult("------Listed homes for " + playerName + "------");
                        string[] listhomes = LPlayerHomes(remoteSteamid, remoteplayerName, out numFoundHomes);
                        p.Message("------" + txt2C + "Listed homes for " + bbclose + txtC + remoteplayerName + bbclose + "[FFFFFF]- " + bbclose + txt2C + "(Max homes: " + bbclose + txtC + numFoundHomes + bbclose + txt2C + ")[-][FFFFFF]-----" + bbclose);
                        foreach (string print in listhomes)
                        {
                            //m_Console.SendResult(print);
                            p.Message(print);
                        }*/
                        return true;
                    }
                    else
                    {
						p.Message(bb._alertC + "You don't have permission to list other playerhomes, listing your own homes instead.");
                        string[] listhomes = LPlayerHomes(p.SteamID, p.Name, out numFoundHomes);
						p.Message("------" + bb._txt2C + "Listed homes for " + bb._cc + bb._txtC + p.Name + bb._cc + "[FFFFFF]- " + bb._cc + bb._txt2C + "(Max homes: " + bb._cc + bb._txtC + numFoundHomes + bb._cc + bb._txt2C + ")[-][FFFFFF]-----" + bb._cc);
                        foreach (string print in listhomes)
                        {
                            //m_Console.SendResult(print);
                            p.Message(print);
                        }
                        return true;
                    }
                }
                catch (Exception e)
                {
                    //Log.Out("Error in Listhomes.RunInternal: _params.Length == 1 " + _params[0] + " invalid " + e);
                    //m_Console.SendResult("Error in Home.RunInternal: _params.Length == 1 " + _params[0] + " invalid " + e);
                    //chatHook.mainChat("Error in Listhomes.RunInternal: _params.Length == 1 " + _params[0] + " invalid " + e, servername, _sender);
                }

            }
            catch (Exception e)
            {
                //Log.Out("Error in Home.RunInternal: " + e);
            }
            return false;
        }
        public static string[] LPlayerHomes(string steamid, string playerName, out int numFoundHomes)
        {
            
            numFoundHomes = 1;
            List<string> listings = new List<string>();
            if (!ReferenceEquals(Homes.Instance.Profiles, null))
            {
                try
                {
                    foreach (var itemNum in Homes.Instance.Profiles)
                    {
                        if (itemNum.SteamID == steamid)
                        {
                            numFoundHomes = itemNum.maxHomes;
                            foreach (var homeID in itemNum.Home)
                            {
								listings.Add("[FF8000]name: '[-][FFFFFF]" + homeID.name + bb._cc + "[FF8000]' - [-][FF8000]x[-][FFFFFF]: " + homeID.x + bb._cc + " [FF8000]y[-][FFFFFF]: " + homeID.y + bb._cc + " [FF8000]z[-][FFFFFF]: " + homeID.z + bb._cc);
                            }
                            string[] ret = listings.ToArray();
                            return ret;
                        }
                    }
                }
                catch (Exception e)
                {
                   // Log.Out("Error in ListHomes.Load: No home found " + e);
                }
            }
            string[] retNone = { "No homes found." };
            return retNone;

        }
    }

}

