﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Text.RegularExpressions;
using System.Xml.Serialization;
using _7DTDManager.Interfaces;
using _7DTDManager.Interfaces.Commands;

namespace _7DTDManager.Commands
{
    using bb = _bbcode;

    public class kz_cmdDelHome : PublicCommandBase
    {
        public kz_cmdDelHome()
        {
            CommandCost = 10;
           // CommandCoolDown = 0;
            CommandHelp = bb._txtC + "Use '" + bb._cmdC + "/delhome <homename>" + bb._txtC + "', use '" + bb._cmdC + "/listhomes" + bb._txtC + "' to find homename";
            CommandName = "delhome";
        }
        public static string _homeName = "";

        public override bool Execute(IServerConnection server, IPlayer p, params string[] args)
        {
            if (args.Length == 2 && args[1] == "?") {
                cmdHelp.QuestionHelp(CommandName, p);
                return false;
            }
            if (args.Length != 2 )
            {
                //   m_Console.SendResult("Use 'delhome <homename>', use 'listhomes' to find homename");
                p.Message(bb._txtC + "Use '" + bb._cmdC + "/delhome <homename>" + bb._txtC + "', use '" + bb._cmdC + "/listhomes" + bb._txtC + "' to find homename");
                return true;
            }
            try
            {
                if (args != null && (args.Length >= 1 && Regex.IsMatch(args[1], @"^[a-zA-Z0-9]+$")))
                {

                    string homename = args[1].ToLower();
                    //  m_Console.SendResult("-"+homename);
                    bool defaultMatch = Regex.Match(homename, "default", RegexOptions.IgnoreCase).Success;

                    if (!defaultMatch)
                    {
                        string returnMessage = DelHomeName(p.SteamID, homename);
                        if (returnMessage != "None")
                        {
                            //  m_Console.SendResult("Deleted: " + homename + " " + returnMessage);
                            p.Message(bb._txtC + "Deleted: '" + bb._txtC + homename + bb._txtC + "', " + returnMessage);
                            Commands.kz_cmdSetHome.save_homeXML();
                        }
                        else
                        {
                            //  m_Console.SendResult("Unable to find or delete: " + homename);
                            p.Message(bb._alertC + "Unable to find or delete: " + bb._txtC + homename);
                        }
                        return true;
                    }
                    else
                    {
                        //  m_Console.SendResult("Cannot delete '" + homename + "' home, just overwrite it with 'sethome'");
                        p.Message("Cannot delete '" + homename + "' home, just overwrite it with 'sethome'");
                        return true;
                    }

                }
            }
            catch (Exception e)
            {
                //Log.Out("Error in Home.RunInternal: _params.Length == 1 " + args[1] + " invalid " + e);
                //p.Message("Error in Home.RunInternal: _params.Length == 1 " + args[1] + " invalid " + e);
                //  m_Console.SendResult("Error in Home.RunInternal: _params.Length == 1 " + _params[0] + " invalid " + e);
            }
            return true;
        }
        public static string DelHomeName(string steamid, string homename)
        {
           // string itemC = chatHook.bb._txtC;
           // string bb._txtC = chatHook.txt2C;
            int numHomes = 0;
            int homeIdToDelete = -1;
            int profileID = 0;
            List<int> homesToKeep = new List<int>();
            if (!ReferenceEquals(Homes.Instance.Profiles, null))
            {
                try
                {
                    int profileCounter = 0;
                    foreach (var itemNum in Homes.Instance.Profiles)
                    {

                        if (itemNum.SteamID == steamid)
                        {
                            profileID = profileCounter;
                            foreach (var homeID in itemNum.Home)
                            {

                                if (homeID.name == homename)
                                {
                                    homeIdToDelete = numHomes;
                                }
                                else
                                {
                                    homesToKeep.Add(numHomes);
                                }
                                numHomes += 1;
                            }
                            if (homeIdToDelete != -1)
                            {
                                //HomesProfileHome[] tempHomes = new HomesProfileHome[newKey];
                                int currentHomes = homesToKeep.Count;
                                HomesProfileHome[] tempHomes = new HomesProfileHome[currentHomes];
                                int key = 0;
                                foreach (int i in homesToKeep)
                                {
                                    tempHomes[key] = new HomesProfileHome();
                                    tempHomes[key].name = Homes.Instance.Profiles[profileID].Home[i].name;
                                    tempHomes[key].x = Homes.Instance.Profiles[profileID].Home[i].x;
                                    tempHomes[key].y = Homes.Instance.Profiles[profileID].Home[i].y;
                                    tempHomes[key].z = Homes.Instance.Profiles[profileID].Home[i].z;
                                    key += 1;
                                }

                                Homes.Instance.Profiles[profileID].Home = new HomesProfileHome[currentHomes];
                                tempHomes.CopyTo(Homes.Instance.Profiles[profileID].Home, 0);
                                int maxhome = Homes.Instance.Profiles[profileID].maxHomes;

                                return "homes left: " + bb._itemC + currentHomes + bb._txtC + "/" + bb._itemC + maxhome; //return number of extra homes
                            }

                        }
                        profileCounter += 1;
                    }
                }
                catch (Exception e)
                {
                  //  Log.Out("Error in DelHome.Load: No home found " + e);
                }
            }

            return "None";

        }
    }

}

