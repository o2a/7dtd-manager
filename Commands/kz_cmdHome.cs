﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Text.RegularExpressions;
using System.Xml.Serialization;
using System.Reflection;
using System.Threading;
using _7DTDManager.Interfaces;
using _7DTDManager.Interfaces.Commands;
using NLog;

namespace _7DTDManager.Commands
{
	using bb = _bbcode;
    public class kz_cmdHome : PublicCommandBase
    {
        static Logger logger = LogManager.GetCurrentClassLogger();
        public kz_cmdHome()
        {
            CommandCost = 20;
           // CommandCoolDown = 0;
            CommandHelp = "Teleports you to your home set with /sethome";
            CommandName = "home";
        }


        public override bool Execute(IServerConnection server, IPlayer p, params string[] args)
        {
            if (args.Length == 2 && args[1] == "?") {
                cmdHelp.QuestionHelp(CommandName, p);
                return false;
            }

            string homeName = string.Empty;
            try
            {

                //set default home
                if (args.Length == 1)
                {
                    //m_Console.SendResult("Teleporting to home");
                    homeName = "default";
                    int[] xyz = Load(p.SteamID, p.Name);
                    //if (xyz != null) {
                    if (xyz[0] != 0 & xyz[1] != 100 & xyz[2] != 0)
                    {

                        //server.Execute("give " + p.EntityID + " splint 1");
                        //server.Execute("tele {0} {1}", p.EntityID, xyz[0] + " " + (xyz[1] + 100 ) + " " + xyz[2]);
                        // HACK: Teleport fix?
                        //Thread.Sleep(500);
                        
                        server.Execute("tele {0} {1}", p.EntityID, xyz[0] + " " + (xyz[1] + 1 ) + " " + xyz[2]);
                        //p.Message("tele {0} {1}", p.EntityID, xyz[0] + " " + xyz[1] + " " + xyz[2]);
						p.Message(bb._txtC + "Teleported you to '[-]" + bb._argC + homeName + bb._cc + bb._txtC + "' home[-]");
                        return true;
                    }
                    else
                    {
                        //m_Console.SendResult("6Error in Home.RunInternal Load xyz");
						p.Message(bb._alertC + "You need to use '[-]" + bb._argC + "/sethome[-]'" + bb._alertC + " before using '[-]" + bb._argC + "/home[-]" + bb._alertC + "'" + bb._cc);
                        return true;
                    }
                    
                }
                if (args.Length >= 1 && Regex.IsMatch(args[1], @"^[a-zA-Z0-9]+$"))
                {
                    homeName = args[1].ToLower().Trim();

                    if (homeName == "")
                    {
                        homeName = "default";
                    }

                    int[] xyz = Load(p.SteamID, p.Name, homeName);
                    if (xyz[0] != 0 & xyz[1] != 100 & xyz[2] != 0)
                    {

                        //server.Execute("give " + p.EntityID + " splint 1");
                        //server.Execute("tele {0} {1}", p.EntityID, xyz[0] + " " +( xyz[1] +100)+ " " + xyz[2]);
                        // HACK: Teleport fix?
                        //Thread.Sleep(500);

                        server.Execute("tele {0} {1}", p.EntityID, xyz[0] + " " + (xyz[1] +1)+ " " + xyz[2]);

                        //p.Message("tele {0} {1}", p.EntityID, xyz[0] + " " + xyz[1] + " " + xyz[2]);
						p.Message(bb._txtC + "Teleported you to '[-]" + bb._argC + homeName + bb._cc + bb._txtC + "' home[-]");
                        return true;
                    }
                    else
                    {
                        //m_Console.SendResult("5Error in Home.RunInternal Load xyz");
                        if (homeName == "default")
                        {
							p.Message(bb._alertC + "You need to use '[-]" + bb._argC + "/sethome[-]'" + bb._alertC + " before using '[-]" + bb._argC + "/home[-]" + bb._alertC + "'" + bb._cc);
                        }
                        else
                        {
							p.Message(bb._errorsC + "Cannot find homename [-][FFFFFF]'" + homeName + "'[-]" + bb._errorsC + " check [-][FFD455]/listhomes[-]");
                        }
                    }
                    return true;
                }
                else
                {

                    //chatHook.mainChat("Error in Home.RunInternal: invalid param only [a-zA-Z0-9] allowed", servername, _sender);

                    //return;
                }

            }
            catch (Exception e)
            {
                //Log.Out("4Error in Home.RunInternal: " + e);
            }

            return true;

        }

        public static void load_homesXML()
        {
            if (File.Exists("playerHomes" + ".xml"))
            {
                try
                {
                    XmlSerializer deserializer = new XmlSerializer(typeof(Homes));
                    TextReader textReader = new StreamReader("playerHomes" + ".xml");

                    Homes.Instance = (Homes)deserializer.Deserialize(textReader);

                    textReader.Close();
                    // return true;
                }
                catch (Exception e)
                {
                    //Log.Out("1Error in Home.loadXML: " + e);
                    Homes.Instance = new Homes();
                    //return true;
                }
            }
            else
            {
                //Log.Out("2Error in Home.loadXML: File Not Found");
                Homes.Instance = new Homes();
                // return true;
            }

        }

        public static int[] Load(string steamid, string playerName, string homeName = "default")
        {

            int[] home_xyz = { 0, 100, 0 };
            try
            {
                foreach (var itemNum in Homes.Instance.Profiles)
                {
                    if (itemNum.SteamID == steamid)
                    {
                        foreach (var homeID in itemNum.Home)
                        {
                            //Log.Out(homeID.name.ToLower() +"==?"+ homeName.ToLower());
                            if (homeID.name.ToLower() == homeName.ToLower())
                            {
                                home_xyz[0] = homeID.x;
                                home_xyz[1] = homeID.y;
                                home_xyz[2] = homeID.z;
                                return home_xyz;
                            }
                        }
                    }
                }
                //return home_xyz;

            }
            catch (Exception e)
            {
               // Log.Out("Error in Home.Load: No home found " + e);
            }
            return home_xyz;
        }
        }

    }
