﻿using _7DTDManager.Interfaces;
using _7DTDManager.Interfaces.Commands;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace _7DTDManager.Commands
{
	using bb = _bbcode;
    public class kz_cmdTeleportDeny : PublicCommandBase
    {

        public kz_cmdTeleportDeny()
        {
            CommandName = "tpdeny";
            CommandHelp = "Deny teleporting to from the requested players teleport.";
            CommandCost = 0;
            //CommandCoolDown = 0;
            CommandAliases = new string[] { "tpd" };
        }
        public override bool Execute(IServerConnection server, IPlayer p, params string[] args)
        {
            if (args.Length == 2 && args[1] == "?") {
                cmdHelp.QuestionHelp(CommandName, p);
                return false;
            }
            try
            {
                foreach (kz_cmdTeleport.telePortQueue queue in kz_cmdTeleport.telePortList.list)
                {
                    if (queue.steamidTo == p.SteamID)
                    {

                        TimeSpan difference = DateTime.Now - queue.timeFrom;
                        double seconds = difference.Seconds;
                        if (seconds > 0 && seconds <= 15)
                        {
                            //valid
                            IPlayer target = null;
                            target = server.AllPlayers.FindPlayerByNameOrID(queue.nameFrom);

                            if (target != null)
                            {

								target.Message(p.Name + "[FF0000], has denied your tp request" + bb._cc);
								p.Message(target.Name + "[FF0000], tp request has been denied" + bb._cc);
                                kz_cmdTeleport.telePortList.list.Remove(queue);
                                return true;
                            }
                            else
                            {
                                //No longer online
								p.Message(queue.nameFrom + "[FF0000], tp request has been denied (also they are not online :)" + bb._cc);
                                kz_cmdTeleport.telePortList.list.Remove(queue);
                                return true;
                            }
                        }
                        else
                        {
							p.Message(queue.nameFrom + "[FFFFFF] tp request (>15seconds ago) expired anyway" + bb._cc);
                            kz_cmdTeleport.telePortList.list.Remove(queue);
                        }

                    }
                }

            }
            catch (Exception e)
            {
                //Log.Out("Error in Teleport_deny.RunInternal: " + e);
            }
            return true;
        }
    }
}
