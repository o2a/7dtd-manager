﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Text.RegularExpressions;
using System.Xml.Serialization;
using _7DTDManager.Interfaces;
using _7DTDManager.Interfaces.Commands;

namespace _7DTDManager.Commands {
    using bb = _bbcode;
    using MySql.Data.MySqlClient;
    using System.Data;
    using NLog;
    public class kz_cmdRankup : PublicCommandBase {
        private static Logger logger = LogManager.GetCurrentClassLogger();
        public kz_cmdRankup() {
            CommandCost = 5;
            //CommandCoolDown = 0;
            CommandHelp = "Use this command to update your ranking in game.";
            CommandName = "rankup";
        }

        public override bool Execute(IServerConnection server, IPlayer p, params string[] args) {
            if (args.Length == 2 && args[1] == "?") {
                cmdHelp.QuestionHelp(CommandName, p);
                return false;
            }
            IPlayer target = null;

            if (args.Length == 2) {

                target = server.AllPlayers.FindPlayerByNameOrID(args[1]);

                if ((target == null) || (!target.IsOnline)) {
                    //p.Message("Targetplayer '{0}' was not found or is not online.", args[1]);
                    p.Message(bb._orange + "Error: Cannot find playername or entity ID: '" + _bbcode._cc + "[FFFFFF]" + args[1] + "[-]" + bb._orange + "'" + _bbcode._cc);

                    return false;
                }
            } else {
                target = p;
            }


            string connectionString_7dtd_server =
           "Server=107.150.27.25;" +
           "Database=7dtd_votes;" +
           "User ID=remote;" +
           "Password=password;" +
           "Pooling=true";


            string connectionString_7dtd_server_local =
"Server=localhost;" +
"Database=Server_7dtd;" +
"User ID=7dtd;" +
"Password=password;" +
"Pooling=true";

            //string steamid = "76561197968560745";

            string totalVotes = "SELECT `username`,`steamid`, COUNT(steamid) " +
"FROM `votes` " +
"WHERE `steamid` = " + target.SteamID + " " +
"GROUP BY `steamid` " +
"ORDER BY `COUNT(steamid)` DESC";


            try {
                using (var con = new MySqlConnection(connectionString_7dtd_server_local))
                using (var cmd = con.CreateCommand()) {
                    con.Open();

                    IDbCommand dbcmd = con.CreateCommand();

                    dbcmd.CommandText = totalVotes;
                    IDataReader reader = dbcmd.ExecuteReader();
                    int Votes = 0;
                    while (reader.Read()) {
                        //string username = (string)reader["username"];
                        //string steamid = (string)reader["steamid"];
                        Votes = int.Parse(reader["COUNT(steamid)"].ToString());
                    }

                    if (Votes >= 10) {
                        //Console.WriteLine("You ranked up, with " + Votes + " votes");
                        //p.Message("You ranked up, with " + Votes + " votes");
                        target.Message(bb._txt3C + "Total server votes: [-][FFFFFF]" + Votes + bb._cc);

                        //target.Message(bb._txt3C + "You now have an extra home that you can custom name: " + bb._cc + bb._cmdC + "/sethome <name>" + bb._cc);

                        if (!ReferenceEquals(Homes.Instance.Profiles, null)) {
                            try {
                                bool foundprofile = false;
                                foreach (var itemNum in Homes.Instance.Profiles) {
                                    if (itemNum.SteamID == target.SteamID) {
                                        foundprofile = true;
                                        if (itemNum.maxHomes == 1) {
                                            itemNum.maxHomes = 2;

                                            target.Message(bb._lightGreen + "You now have " + bb._cc + bb._white + itemNum.maxHomes + bb._cc + bb._lightGreen + " homes, more details check " + bb._cc + bb._white + "/listhomes" + bb._cc);
                                            kz_cmdSetHome.save_homeXML();
                                        }
                                        if (itemNum.maxHomes == 2) {
                                            target.Message(bb._lightGreen + "You have already " + bb._cc + bb._white + itemNum.maxHomes + bb._cc + bb._lightGreen + " homes, further ranks have yet to be added consider donating to help server." + bb._cc);

                                        }
                                    }
                                }
                                if (foundprofile == false) {

                                    target.Message(bb._warningsC + "Just do " + bb._cc + bb._cmdC + "/sethome" + bb._cc + bb._warningsC + " to set your first home then " + bb._cc + bb._cmdC + "/rankup" + bb._cc + bb._warningsC + " again to gain an extra home" + bb._cc);
                                }
                            } catch (Exception e) {
                                logger.Error("Error1 kz_cmdRankup: " + e.Message);
                            }

                        }

                    } else {
                        int votesNeeded = ((Votes - 10) * -1);

                        target.Message(bb._lightOrange + " you still need " + ((votesNeeded > 1) ? "another " + bb._cc + bb._txtC + votesNeeded + bb._cc + bb._lightOrange + " votes" : bb._cc + bb._txtC + votesNeeded + bb._cc + bb._lightOrange + "more vote") + " to rank up" + bb._cc);

                    }

                    cmd.Dispose();
                    con.Close();
                }

            } catch (Exception e) {
                logger.Error("Error2 kz_cmdRankup: " + e.Message);
            }

            return false;
        }

    }

}

