﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Text.RegularExpressions;
using System.Xml.Serialization;
using _7DTDManager.Interfaces;
using _7DTDManager.Interfaces.Commands;
using System.Threading;
using _7DTDManager.Objects;

namespace _7DTDManager.Commands
{
	using bb = _bbcode;
    public class kz_cmdIdList : PublicCommandBase
    {
        public kz_cmdIdList()
        {
            CommandCost = 5;
           // CommandCoolDown = 0;
            CommandHelp = "Shows a list of players idnumbers... easier to use for commands if playername is (*^$%!&";
            CommandName = "idlist";
        }

        public override bool Execute(IServerConnection server, IPlayer p, params string[] args)
        {
            if (args.Length == 2 && args[1] == "?") {
                cmdHelp.QuestionHelp(CommandName, p);
                return false;
            }
            


            bool showSteamID = false;
            bool totaltime = false;
            
            if (p.IsAdmin)
            {
                if ((args.Length > 1) && (args[1].Equals("steam")))
                {
                    showSteamID = true;
                }
                if ((args.Length > 1) && (args[1].Equals("all")))
                {
                    showSteamID = true;
                    totaltime = true;
                }
            }

            if ((args.Length > 1) && (args[1].Equals("time")))
            {
                totaltime = true;
            }

            int playerX = (int)Math.Round(p.CurrentPosition.X);
            int playerY = (int)Math.Round(p.CurrentPosition.Y);
            int playerZ = (int)Math.Round(p.CurrentPosition.Z);




            //bool found = false;
            List<IdListData> idList = new List<IdListData>();

            var AllPlayers = server.AllPlayers.Players;
            foreach (var currentPlayer in AllPlayers)
            {
                if(currentPlayer.IsOnline){

                //Console.WriteLine(currentPlayer.Name);

                    string steamID = currentPlayer.SteamID;
                    string onlineTime = string.Empty;
                    string sessionAge = string.Empty;
                    if (totaltime){
                        onlineTime = " [FF552A]Total Time: [-][FFFFFF] " + (currentPlayer.Age).ToString() + " minutes[-]";
                    }
                        sessionAge = " [FF552A]Session Time: [-][FFFFFF] " + (currentPlayer.SessionAge).ToString() + " minutes[-]";
                    


                    string ip = currentPlayer.IPAddress;
					string countryName = "";
					string countryCode = "";
					
					//Get Country from IP
					if (ip != "127.0.0.1" && ip != "") {
						try {
							//open the database
							GeoIP.LookupService ls = new GeoIP.LookupService("GeoIP.dat", GeoIP.LookupService.GEOIP_MEMORY_CACHE);
							//Log.Out(GamePrefs.GetString(EnumGamePrefs.SaveGameFolder) + "/" + "GeoIP.dat");
							//get country of the ip address
							if (ls != null) {
								GeoIP.Country c = ls.getCountry(ip);
								countryName = c.getName();
								countryCode = c.getCode();								
							}
						} catch (Exception e) {
						Console.WriteLine("Error in cmdIdList.RequestToSpawnPlayer.FileExists.GeoIP: " + e);
						}
					}
                    idList.Add(new IdListData(currentPlayer.Name, currentPlayer.EntityID, steamID, onlineTime,sessionAge, countryName, countryCode));
                    }
                }



            idList.Sort(delegate(IdListData i1, IdListData i2) { return i1.UserName.CompareTo(i2.UserName); });
            //Console.WriteLine("Sorted list, by distance");
            idList.ForEach(delegate(IdListData iD)
            {
                string country = "";
                if (!iD.CountryName.Equals("N/A") && !iD.CountryCode.Equals(""))
                {
                    //country = "[FFAA2A], Country: [-][7FFF55]" + iD.CountryName + " [-][CCCCCC](" + iD.CountryCode + ")[-]";
                    country = " [-][CCCCCC](" + iD.CountryName + ")[-]";
                }

               //string message = "[FF552A]" + iD.UserName + "[-][FFAA2A] ID: [-][FFFFFF]" + iD.EntityID + "[-]" + country + ", " + iD.SessionTime;
                string message = "[FF552A]" + iD.UserName +country+ "[FFAA2A] ID: [-][FFFFFF]" + iD.EntityID + "[-], " + iD.SessionTime;
                if (showSteamID)
                {
                    message = "[FF552A]" + iD.UserName + country+"[FFAA2A] ID: [-][FFFFFF]" + iD.EntityID + "," + iD.SteamID + "[-]";
                }
                if (showSteamID && totaltime)
                {
                    message = "[FF552A]" + iD.UserName + country +"[FFAA2A] ID: [-][FFFFFF]" + iD.EntityID + ", " + iD.SteamID + "[-], " + iD.OnlineTime;
                }
                p.Message(message);
            });

            return false;
        }


        public class IdListData
        {
            public IdListData(string userName, string entityID, string steamID, string onlinetime, string sessiontime, string countryName, string countryCode)
            {
                this.UserName = userName;
                this.EntityID = entityID;
                this.SteamID = steamID;
                this.OnlineTime = onlinetime;
                this.SessionTime = sessiontime;
                this.CountryName = countryName;
                this.CountryCode = countryCode;
            }
            public string UserName { set; get; }
            public string EntityID { set; get; }
            public string SteamID { set; get; }
            public string OnlineTime { set; get; }
            public string SessionTime { set; get; }
            public string CountryName { set; get; }
            public string CountryCode { set; get; }
        }
     
    }

}