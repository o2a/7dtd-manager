﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Text.RegularExpressions;
using System.Xml.Serialization;
using _7DTDManager.Interfaces;
using _7DTDManager.Interfaces.Commands;
using System.Threading;
using _7DTDManager.Objects;

namespace _7DTDManager.Commands
{
	using bb = _bbcode;
    public class kz_cmdWho : PublicCommandBase
    {
        public kz_cmdWho()
        {
            CommandCost = 5;
            //CommandCoolDown = 0;
            CommandHelp = "Shows a list of nearby players /who <optional max distance max 250 regulars>";
            CommandName = "who";
        }

        public override bool Execute(IServerConnection server, IPlayer p, params string[] args)
        {
            server.Execute("lp");
            Console.WriteLine("WHO//Player current position: "+p.CurrentPosition);

            Thread t = new Thread(() => {
                Thread.Sleep(800); //1 second delay
                Console.WriteLine("WHO.thread//Player current position: " + p.CurrentPosition);
                if (args.Length == 2 && args[1] == "?") {
                    cmdHelp.QuestionHelp(CommandName, p);
                    //return false;
                }
                string bbclose = "[-]";


                //var onlinePlayers = from p in server.allPlayers where p.IsOnline select p;
                var allPlayers = server.AllPlayers.Players;


                int areasize = 250;
                // bool isAdmin = false;

                if (p.IsAdmin) {
                    areasize = 100000;
                }

                bool showCords = false;
                if (args.Length == 1) {
                    //Nothing
                } else {
                    if ((args.Length > 1) || (args[1].Length != 0)) {
                        if (int.TryParse(args[1], out areasize)) {
                            if (!p.IsAdmin && areasize > 250) {
                                areasize = 250;
                                p.Message(bb._alertC + "Max " + bbclose + bb._cmdC + "/who <area>" + bbclose + bb._alertC + " size is '250', you put: " + bb._cc + bb._txtC + areasize + bb._cc);
                            }
                        } else {
                            if (args[1].Equals("cords")) {
                                showCords = true;
                                p.Message("showing cords");
                            } else {
                                p.Message("/who <area num>|'cords'");
                            }
                        }
                    }
                }
                //if (_params.Length != 1 || _params[0].Length == 0) {

                //    if (_params.Length != 1 && _params[1].Length != 0) {
                //        if (_params[0] != null) {
                //            if (int.TryParse(_params[0], out areasize)) {
                //                if (areasize > 250) {
                //                    areasize = 100;
                //                    chatHook.mainChat("Max '/who <area>' size is 250, you put:" + areasize, servername, _sender);
                //                }
                //            }
                //        }
                //    }

                int playerX = (int)Math.Round(p.CurrentPosition.X);
                int playerY = (int)Math.Round(p.CurrentPosition.Y);
                int playerZ = (int)Math.Round(p.CurrentPosition.Z);




                //bool found = false;
                List<WhoData> whoList = new List<WhoData>();

                foreach (var currentPlayer in allPlayers) {
                    // Console.WriteLine(currentPlayer.Name);

                    //if (currentPlayer.IsOnline && currentPlayer.Name != p.Name && !p.IsAdmin)
                    if (currentPlayer.IsOnline && currentPlayer.Name != p.Name && !currentPlayer.IsAdmin) {
                        /*
                        int Player_AdminLevel = currentPlayer.AdminLevel;
                        int Player_Age = currentPlayer.Age;
                        int Player_BloodCoins = currentPlayer.BloodCoins;
                        int Player_Bounty = currentPlayer.Bounty;
                        int Player_BountyCollected = currentPlayer.BountyCollected;
                        double Player_CurrentPosition_X = currentPlayer.CurrentPosition.X;
                        double Player_CurrentPosition_Y = currentPlayer.CurrentPosition.Y;
                        double Player_CurrentPosition_Z = currentPlayer.CurrentPosition.Z;
                        int Player_Deaths = currentPlayer.Deaths;
                        double Player_DistanceTravelled = currentPlayer.DistanceTravelled;
                        string Player_EntityID = currentPlayer.EntityID;
                        DateTime Player_FirstLogin = currentPlayer.FirstLogin;
                        IReadOnlyList<IPlayer> Player_Friends_Items = currentPlayer.Friends.Items;

                 foreach (var friend in Player_Friends_Items)
                        {
                            string friendName = friend.Name;
                            if (p.IsAdmin)
                            {
                                p.Message(currentPlayer.Name + " friends with " + friendName);
                            }
                        }   

                        string Player_IPAddress = currentPlayer.IPAddress;
                        bool Player_IsAdmin = currentPlayer.IsAdmin;
                        bool Player_IsOnline = currentPlayer.IsOnline;
                        IReadOnlyList<IPosition> Player_LandProtections_Items = currentPlayer.LandProtections.Items;
                        DateTime Player_LastLogin = currentPlayer.LastLogin;
                        DateTime Player_LastPayday = currentPlayer.LastPayday;
                        IReadOnlyList<IMailMessage> Player_Mailbox_Mails = currentPlayer.Mailbox.Mails;
                        string Player_Name = currentPlayer.Name;
                        int Player_Ping = currentPlayer.Ping;
                        int Player_PlayerKills = currentPlayer.PlayerKills;
                        int Player_Spent = currentPlayer.Spent;
                        string Player_SteamID = currentPlayer.SteamID;
                        int Player_zCoins = currentPlayer.zCoins;
                        int Player_ZombieKills = currentPlayer.ZombieKills;
                        */

                        // chatHook.mainChat("TotalPlayTime" + players.TotalPlayTime, servername, _sender);

                        int wx = (int)Math.Round(currentPlayer.CurrentPosition.X);
                        int wy = (int)Math.Round(currentPlayer.CurrentPosition.Y);
                        int wz = (int)Math.Round(currentPlayer.CurrentPosition.Z);
                        if (p.IsAdmin) {
                            //  p.Message("Who.RunInternal.Loop: Name:" + currentPlayer.Name + " x:" + wx + " y:" + wy + " z:" + wz);
                        }
                        int checkX = areaCheck(playerX, wx, areasize);
                        int checkY = areaCheck(playerY, wy, areasize + 300);
                        int checkZ = areaCheck(playerZ, wz, areasize);

                        if ((checkX != 111111) && (checkZ != 111111)) {
                            string eastwest = "";
                            string northsouth = "";
                            //#x -west/east+   
                            if (wx < 0) {
                                eastwest = Math.Abs(wx) + " West";
                            } else {
                                eastwest = wx + " East";
                            }
                            //#z -South/North+   
                            if (wz < 0) {
                                northsouth = Math.Abs(wz) + " South";
                            } else {
                                northsouth = wz + " North";
                            }

                            int x1 = wx;
                            int y1 = wz;
                            int x2 = playerX;
                            int y2 = playerZ;

                            var radians = Math.Atan2((y1 - y2), (x1 - x2));
                            var compassReading = radians * (180 / Math.PI);

                            if (compassReading > 90) {
                                compassReading = 450 - compassReading;
                            } else {
                                compassReading = 90 - compassReading;
                            }

                            string compass = getCompassDirection(compassReading);

                            double distance = Math.Sqrt(Math.Pow((wx - playerX), 2) + Math.Pow((wy - playerY), 2) + Math.Pow((wz - playerZ), 2));
                            int distanceInt = Convert.ToInt32(distance);
                            whoList.Add(new WhoData(currentPlayer.Name, currentPlayer.SteamID, distanceInt, compass, wx, wy, wz));
                        }
                    }
                }

                if (whoList.Count != 0) {
                    whoList.Sort(delegate(WhoData w1, WhoData w2) { return w2.DistanceInt.CompareTo(w1.DistanceInt); });
                    //Console.WriteLine("Sorted list, by distance");
                    whoList.ForEach(delegate(WhoData wD) {

                        if (showCords) {
                            p.Message("[7FFF55]" + wD.UserName + bbclose + "[D4FF7F] is [-][FFD455]'" + wD.DistanceInt + "'[-][FF7F2A] blocks away! ([-][FFD455]" + wD.Compass + bbclose + "[FF7F2A]) // [-][FF8000]x  [-][FFFFFF]" + wD.X + ", [-][FF8000]y  [-][FFFFFF]" + wD.Y + ", [-][FF8000]z  [-][FFFFFF]" + wD.Z + bbclose);
                        } else {
                            // chatHook.mainChat("[7FFF55]" + players.Name + "[D4FF7F] is [FFD455]'" + distanceInt + "'[FF7F2A] blocks away! ([FFD455]" + compass + "[FF7F2A]) // [FF8000]x  [FFFFFF]" + wx + "(" + checkX + "), [FF8000]y  [FFFFFF]" + wy + "(" + checkY + "), [FF8000]z  [FFFFFF]" + wz + "(" + checkZ + ")", servername, _sender);
                            p.Message("[7FFF55]" + wD.UserName + bbclose + "[D4FF7F] is [-][FFD455]'" + wD.DistanceInt + "'[-][FF7F2A] blocks away! '[-][FFD455]" + wD.Compass + bbclose + "[FF7F2A]' from you." + bbclose);
                        }

                    });
                   // return true;
                } else {
                    p.Message("[FF552A]No one found nearby you in the radius of: " + areasize + bbclose);
                    //return false;
                }

            });
            t.IsBackground = true;
            t.Start();


            return true;
        }

                
            
        public class WhoData
        {
            public WhoData(string userName, string steamId, int distanceInt, string compass, int x, int y, int z)
            {
                this.UserName = userName;
                this.SteamId = steamId;
                this.DistanceInt = distanceInt;
                this.Compass = compass;

                this.X = x;
                this.Y = y;
                this.Z = z;
            }
            public string UserName { set; get; }
            public string SteamId { set; get; }
            public int DistanceInt { set; get; }
            public string Compass { set; get; }
            public int X { set; get; }
            public int Y { set; get; }
            public int Z { set; get; }
        }
        public static string getCompassDirection(double bearing)
        {
            int tmp = 0;
            try
            {
                tmp = (int)Math.Round(bearing / 45);
                string[] compasspoints = new string[] { "N", "NE", "E", "SE", "S", "SW", "W", "NW", "N" };
                //string[] compasspoints = new string[] { "N", "NE", "E", "SE", "S", "SW", "W", "NW" };

                return compasspoints[tmp];
            }
            catch (Exception e)
            {
               // Log.Out("Error in Who.RunInternal: bearing: " + bearing.ToString() + " tmp: " + tmp.ToString() + " error: " + e);
                return "FAIL";
            }
        }
        public static int areaCheck(int player, int playerX, int areasize)
        {
            if ((player <= playerX) && (player >= (playerX - areasize)))
            {
                int data = areasize - (player - (playerX - areasize));
                return data;
            }
            if ((player >= playerX) && (player <= (playerX + areasize)))
            {
                int data = ((playerX + areasize) - player) - areasize;
                return data;
            }
            return 111111;
        }
    }

}