﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Text.RegularExpressions;
using System.Xml.Serialization;
using _7DTDManager.Interfaces;
using _7DTDManager.Interfaces.Commands;
using System.Threading;
using _7DTDManager.Objects;

namespace _7DTDManager.Commands
{
	using bb = _bbcode;
    public class kz_cmdWarps : PublicCommandBase
    {
        public kz_cmdWarps()
        {
            CommandCost = 10;
            //CommandCoolDown = 0;
            CommandHelp = "Warp to another area /warp for list and /warp <name>, to teleport ";
            CommandName = "warpdisabled";
        }

        public override bool Execute(IServerConnection server, IPlayer p, params string[] args)
        {
            if (args.Length == 2 && args[1] == "?") {
                cmdHelp.QuestionHelp(CommandName, p);
                return false;
            }

            string bbclose = "[-]";
            
                int x = (int)p.CurrentPosition.X;
                int y = (int)p.CurrentPosition.Y;
                int z = (int)p.CurrentPosition.Z;


                if (args.Length < 2)
                {
                    //chatHook.mainChat("[26C9FF] ----------", servername, _sender);
				p.Message("[26C9FF] Warps: [-][FFFFFF]/warp craptown [-][00B9F2]| [-][FFFFFF]forest [00B9F2]| [-][FFFFFF]desert" + bb._cc);
				p.Message("[26C9FF] Warps: [-][FFFFFF]/warp grasslands [-][00B9F2]| [-][FFFFFF]mainshitty [-][00B9F2]| [-][FFFFFF]plains" + bb._cc);
                    return false;
                }


                if (args.Length >= 2)
                {

                    switch (args[1].ToLower())
                    {
                        case "list":
                            p.Message("[26C9FF] Warps: [-][FFFFFF]/warp craptown [-][00B9F2]| [-][FFFFFF]forest [-][00B9F2]| [-][FFFFFF]desert[-]");
                            p.Message("[26C9FF] Warps: [-][FFFFFF]/warp mainshitty [-][00B9F2]| [-][FFFFFF]<suggest> [-][00B9F2]| [-][FFFFFF]<suggest>[-]");
                            //chatHook.mainChat("[26C9FF] Warps: [FFFFFF]/warp grasslands [00B9F2]| [FFFFFF]desert [00B9F2]| [FFFFFF]church", servername);
                            //chatHook.mainChat("[26C9FF] Warps: [FFFFFF]/warp hotel [00B9F2]| [FFFFFF]<suggest> [00B9F2]| [FFFFFF]<suggest>", servername);
                            return false;
                        case "craptown":
                           kz_cmdTeleport.telePort(-350, 66, -607, "craptown",p.Name, p,server);
                            return true;
                        case "forest":
                            kz_cmdTeleport.telePort(2489, 96, 664, "forest", p.Name, p, server);
                            return true;
                        case "grasslands":
                            kz_cmdTeleport.telePort(-767, 68, -303, "grasslands", p.Name, p, server);
                            return true;
                        case "desert":
                            kz_cmdTeleport.telePort(4496, 68, 2469, "desert", p.Name, p, server);
                            return true;
                        case "mainshitty":
                            kz_cmdTeleport.telePort(65, 66, 66, "mainshitty", p.Name, p, server);
                            return true;
                        case "plains":
                            kz_cmdTeleport.telePort(1998, 67, 587, "plains", p.Name, p, server);
                            return true;
                        case "thunderdrome-":
                            goto case "pvp-";
                        // telePort(pvpX[rXYZ], pvpY[rXYZ], pvpZ[rXYZ], _params[0], playerName, servername, _sender, p1);
                        //return;
                        case "pvp-":
                           // Random rnd = new Random();
                           // int rXYZ = rnd.Next(0, 5);
                           // telePort(pvpX[rXYZ], pvpY[rXYZ], pvpZ[rXYZ], _params[0], playerName, servername, _sender, p1);
                           // if (rXYZ == 1) { chatHook.mainChat("ThunderDrome (Deathmatch-FFA), by Sarlron13", servername, _sender); }

                            return false;
                    }

                    p.Message("[26C9FF] Warp: [-][FFFFFF]'" + args[1] + "' not found, did you spell it exactly? check /warps" + bbclose);

                }
            return false;
        }

    }
}

