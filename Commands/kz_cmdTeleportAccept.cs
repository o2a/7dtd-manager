﻿using _7DTDManager.Interfaces;
using _7DTDManager.Interfaces.Commands;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;

namespace _7DTDManager.Commands {
    using bb = _bbcode;
    public class kz_cmdTeleportAccept : PublicCommandBase {

        public kz_cmdTeleportAccept() {
            CommandName = "tpaccept";
            CommandHelp = "Allow teleport from another players teleport request.";
            CommandCost = 10;
            //CommandCoolDown = 0;
            CommandAliases = new string[] { "tpa" };
        }
        public override bool Execute(IServerConnection server, IPlayer p, params string[] args) {
            if (args.Length == 2 && args[1] == "?") {
                cmdHelp.QuestionHelp(CommandName, p);
                return false;
            }
            try {

                bool foundtp = false;
                foreach (kz_cmdTeleport.telePortQueue queue in kz_cmdTeleport.telePortList.list) {
                    if (queue.steamidTo == p.SteamID) {
                        foundtp = true;
                        //TODO: Add timestamp check, queue item older than 15sec gets wiped from the list.

                        TimeSpan difference = DateTime.Now - queue.timeFrom;
                        double seconds = difference.Seconds;
                        if (seconds > 0 && seconds <= 15) {
                            //valid
                            IPlayer target = null;
                            target = server.AllPlayers.FindPlayerByNameOrID(queue.nameFrom);

                            if (target != null) {
                                
                                p.PlayerPositionUpdated += (sender2, e2) => p_PlayerPositionUpdated(sender2, e2, target, queue, server);

                                server.Execute("lp");
                                  
                                /*
                                p.UpdateHomePosition(p.CurrentPosition);

                                int x = (int)p.CurrentPosition.X;
                                int y = (int)p.CurrentPosition.Y;
                                int z = (int)p.CurrentPosition.Z;

                                kz_cmdTeleport.telePort(x, y, z, p.Name, target.Name, target, server);
                                kz_cmdTeleport.telePortList.list.Remove(queue);
                                */

                                return true;
                            } else {
                                //No longer online
                                p.Message(queue.nameFrom + "[FF552A] is no longer online.[-]");
                                kz_cmdTeleport.telePortList.list.Remove(queue);
                                return true;
                            }
                        } else {
                            p.Message(queue.nameFrom + "[FFAA2A] needs to resend the tp request [-][FF552A](you only have 15sec to accept teleport [-][37EF3A]/tpa[-][FF552A])[-]");
                            kz_cmdTeleport.telePortList.list.Remove(queue);
                        }

                    } if (!foundtp) {
                        p.Message("[FFAA55]No teleport requests in queue, ask someone to /tp to you :)[-]");

                    }
                }

            } catch (Exception e) {
                //  Log.Out("Error in Teleport_allow.RunInternal: " + e);
            }
            return true;
        }
        void p_PlayerPositionUpdated(object sender, PlayerPositionUpdateEventArgs e, IPlayer target, kz_cmdTeleport.telePortQueue queue, IServerConnection server) {

            IPlayer p = sender as IPlayer;


            p.UpdateHomePosition(p.CurrentPosition);

            int x = (int)p.CurrentPosition.X;
            int y = (int)p.CurrentPosition.Y;
            int z = (int)p.CurrentPosition.Z;

            kz_cmdTeleport.telePort(x, y, z, p.Name, target.Name, target, server);
            kz_cmdTeleport.telePortList.list.Remove(queue);

            /* 
             server.Execute("lp");

            Thread t = new Thread(() => {
                Thread.Sleep(500); //1 second delay

            });
            t.IsBackground = true;
            t.Start();
             */

            // Now here we can process the real command, like setting the home
            // No need to care for unsubscribiung from the event. The Manager does that for us

        }
    }
}
