﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Text.RegularExpressions;
using System.Xml.Serialization;
using _7DTDManager.Interfaces;
using _7DTDManager.Interfaces.Commands;

namespace _7DTDManager.Commands
{
	using bb = _bbcode;
    public class kz_cmdSetHome : PublicCommandBase
    {
        public kz_cmdSetHome()
        {
            CommandCost = 60;
            //CommandCoolDown = 0;
            CommandHelp = "Set the position you will teleport to using /home";
            CommandName = "sethome";
        }
        public static string _homeName = "";

        public override bool Execute(IServerConnection server, IPlayer p, params string[] args)
        {
           // p.PlayerMoved += player_PlayerMoved;
            //server.Execute("lp");
            if (args.Length == 2 && args[1] == "?") {
                cmdHelp.QuestionHelp(CommandName, p);
                return false;
            }

            if (!p.CurrentPosition.IsValid)
            {
                p.Message("No valid position for you recorded. Wait a little please.");
                return false;
            }

            //p.Message("Homeposition set to {0}", p.HomePosition.ToHumanString());
            //return true;

            /*
            IPlayer otherPlayer = server.allPlayers.FindPlayerbyName(send);
            if (otherPlayer != null)
                otherPlayer.AddCoins(-1000, "Becasue you are dumb");
            */


            /*
            double x = p.CurrentPosition.X;
            double y = p.CurrentPosition.Y;
            double z = p.CurrentPosition.Z;
            */



            server.Execute("lp");
            p.UpdateHomePosition(p.CurrentPosition);


            int x = (int)p.CurrentPosition.X;
            int y = (int)p.CurrentPosition.Y;
            int z = (int)p.CurrentPosition.Z;

            int maxHomes = 0;
            string homeName = "default";
            bool homeSet = false;
            //set default home
            if (args.Length == 1)
            {
                //m_Console.SendResult ("Home set, use \"home\" to return back.");
				p.Message(bb._txtC + "Home set, use '[-]" + bb._cmdC + "/home[-]" + bb._txtC + "' to return back.[-]");
                Save(p.SteamID, p.Name, x, y, z, out maxHomes, out homeSet, p, homeName);

                save_homeXML();
                return true;
            }
            if (args.Length > 1 && Regex.IsMatch(args[1], @"^[a-zA-Z0-9]+$"))
            {
                //if (_params != null && (_params.Length >= 0 && Regex.IsMatch(_params[0], @"^[a-zA-Z0-9]+$"))) {
                homeName = args[1].ToLower().Trim();

                if (homeName == "")
                {
                    homeName = "default";
                }


                Save(p.SteamID, p.Name, x, y, z, out maxHomes, out homeSet, p, homeName);

                //m_Console.SendResult ("Home set, use \"home "+homeName+"\" to return back.");

                //Check to see if the homename was changed to 'default'
                if (homeName != _homeName)
                {
					p.Message(bb._txtC + "You can't have a named home for your first home, so '[-]" + bb._argC + homeName +bb._cc+ bb._txtC + "' will be shown as '[-]" + bb._argC + "default[-]" + bb._txtC + "' in [-]" + bb._cmdC + "/listhomes[-]");
					p.Message(bb._txtC + "Home set, use '[-]" + bb._cmdC + "/home[-]" + bb._txtC + "' to teleport back ([-]" + bb._cmdC + "/sethome[-]" + bb._txtC + " to change it).[-]");
                }
                else
                {
                    if (homeSet)
                    {
						p.Message(bb._txtC + "Home set, use '[-]" + bb._cmdC + "/home [-]" + bb._argC + _homeName + bb._cc + bb._txtC + "' to return back.[-]");
                    }
                }
                save_homeXML();
                return true;
            }
            else
            {
                //m_Console.SendResult ("sethome \""+_params[0]+"\" is invalid, only [a-zA-Z0-9] allowed");
				p.Message(bb._alertC + "sethome '[-]" + bb._argC + args[1] + bb._cc + bb._alertC + "' is invalid, only [a-zA-Z0-9] allowed" + bb._cc);
                return false;
            }

        }


     
    public void player_PlayerMoved(object sender, PlayerMovementEventArgs e)
   {
         IPlayer p = sender as IPlayer;
         p.PlayerMoved -= player_PlayerMoved;
         p.UpdateHomePosition(e.NewPosition);

    }


public static void save_homeXML()
        {
            string playerHome = "playerHomes/";
            playerHome = "";
            try {
                Homes saveObj = Homes.Instance;
                //Directory.CreateDirectory(GamePrefs.GetString(EnumGamePrefs.SaveGameFolder) + "/playerHomes");
                XmlSerializer serializer = new XmlSerializer(typeof(Homes));
                TextWriter textWriter = new StreamWriter("playerHomes" + ".xml");
                serializer.Serialize(textWriter, saveObj);
                textWriter.Close();
            } catch (Exception e) {

               // Log.Out("Error in Sethome.saveXML: " + e);
            }
        }
public static void Save(string steamid, string playerName, int x, int y, int z, out int maxHomes, out bool homeSet, IPlayer p, string homeName = "default")
        {
            homeSet = false;
            _homeName = homeName;
            Commands.kz_cmdHome.load_homesXML();

            //string servername = "ᴷᶻ[EF1A45]»[FFFFFF]";
            
            bool foundprofile = false;
            maxHomes = 0;
            int numFoundHomes = 0;
            
            bool itsNull = false;
            if (!ReferenceEquals(Homes.Instance.Profiles, null)) {
            try {
                foreach (var itemNum in Homes.Instance.Profiles) {
                    if (itemNum.SteamID == steamid) {
                        foundprofile = true;
                        maxHomes = itemNum.maxHomes; 
                        //TODO: null check on Home  //if (!ReferenceEquals(Homes.Instance.Profiles, null)) {
                        foreach (var homeID in itemNum.Home) {
                            numFoundHomes += 1;
                            if (homeID.name == _homeName) {

                                homeID.x = x;
                                homeID.y = y;
                                homeID.z = z;
                                homeSet = true;
                                return;
                            }
                        }
                        // int num = itemNum.Home.Length; //using foundprofilehomes
                         if(itemNum.maxHomes > numFoundHomes){
                            //First home always has to be default
                            if (numFoundHomes == 0) {
                                if (_homeName != "default") {
                                    _homeName = "default";
                                }
                            }
                            HomesProfileHome[] newHome = new HomesProfileHome[numFoundHomes + 1];
                            newHome[numFoundHomes] = new HomesProfileHome();
                            newHome[numFoundHomes].name = _homeName;
                            newHome[numFoundHomes].x = x;
                            newHome[numFoundHomes].y = y;
                            newHome[numFoundHomes].z = z;

                            itemNum.Home.CopyTo(newHome, 0);

                            itemNum.Home = new HomesProfileHome[numFoundHomes + 1];
                            newHome.CopyTo(itemNum.Home, 0);
                            //string test = itemNum.Home[1].name;
                            homeSet = true;
                            return;
                        } else {
                           // m_Console.SendResult("Your maxhomes(" + itemNum.maxHomes.ToString() + ") has been reached (ask Koolio for more) or use \"sethome <existing homename>\" or delete an existing home name \"delhome <name>\" before making a new one with a different name.");
								p.Message(bb._alertC + "Your maxhomes " + bb._cc + bb._argC + itemNum.maxHomes.ToString() + bb._cc + bb._alertC + " has been reached (vote and use /rankup or donate) or use '" + bb._cc + bb._cmdC + "/sethome <existing homename>" + bb._cc + bb._alertC + "' or delete an existing home name '" + bb._cc + bb._cmdC + "/delhome <homename>" + bb._cc + bb._alertC + "' before making a new one with another name." + bb._cc);
                            homeSet = false;
                            return;
                        }
                    }
                }

            } catch (Exception e) {
               // Log.Out("Error in Sethome.Save: " + e);

            }
             }else { 
            Console.WriteLine("Homes.Instance.Profiles is null");
            itsNull = true;
        }
            if (foundprofile == false) {
                try {
                    //First home always has to be default
                    if (numFoundHomes == 0) {
                        if (_homeName != "default") {
                            _homeName = "default";
                        }
                    }
                    HomesProfile newProfile = new HomesProfile();
                    newProfile.PlayerName = playerName;
                    newProfile.SteamID = steamid;
                    newProfile.maxHomes = 1;

                    HomesProfileHome newHome = new HomesProfileHome();
                    newHome.name = _homeName;
                    newHome.x = x;
                    newHome.y = y;
                    newHome.z = z;

                    newProfile.Home = new HomesProfileHome[1];
                    newProfile.Home[0] = newHome;

                    int num = 0;
                    if (!itsNull) {
                        num = Homes.Instance.Profiles.Length;
                        HomesProfile[] tempHomeProfiles = new HomesProfile[num + 1];
                        Homes.Instance.Profiles.CopyTo(tempHomeProfiles, 0);
                        //tempHomeProfiles[num+1] causes null..  very weird
                        tempHomeProfiles[num] = newProfile;
                        Homes.Instance = new Homes();
                        Homes.Instance.Profiles = new HomesProfile[num + 1];
                        tempHomeProfiles.CopyTo(Homes.Instance.Profiles, 0);
                    } else {
                        HomesProfile[] tempHomeProfiles = new HomesProfile[num + 1];
                        tempHomeProfiles[num] = newProfile;
                        Homes.Instance = new Homes();
                        Homes.Instance.Profiles = new HomesProfile[num + 1];
                        tempHomeProfiles.CopyTo(Homes.Instance.Profiles, 0);
                    }
                    homeSet = true;
                   // Log.Out(Homes.Instance.Profiles[0].PlayerName);

                } catch (Exception e) {
                   // Log.Out("3Error in SetHome.Save else" + e);

                }
            }
   }
        }
}

