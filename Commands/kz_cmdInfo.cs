﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Text.RegularExpressions;
using System.Xml.Serialization;
using _7DTDManager.Interfaces;
using _7DTDManager.Interfaces.Commands;
using System.Threading;
using _7DTDManager.Objects;

namespace _7DTDManager.Commands
{
    public class kz_cmdInfo : PublicCommandBase
    {
        public kz_cmdInfo()
        {
            CommandCost = 5;
           //CommandCoolDown = 0;
            CommandHelp = "Shows info (landclaim owners etc) about the area you are standing in";
            CommandName = "info";
        }
        public static string[] _args = { };
        public static IReadOnlyList<IPlayer> _allPlayers = null;

        public override bool Execute(IServerConnection server, IPlayer p, params string[] args) {

            if (args.Length == 2 && args[1] == "?") {
                cmdHelp.QuestionHelp(CommandName, p);
                return false;
            }
            _args = args;
            _allPlayers = server.AllPlayers.Players;

            p.PlayerPositionUpdated += p_PlayerPositionUpdated;
            server.Execute("llp");
            server.Execute("lp");
            return true;

            
        }
        public void p_PlayerPositionUpdated(object sender, PlayerPositionUpdateEventArgs e)
        {

            IPlayer p = sender as IPlayer;
            IServerConnection server = sender as IServerConnection;

            bool showCords = false;
            bool showRegion = false;

            if ((_args.Length == 2))
            {
                if (_args[1].Equals("cords"))
                {
                    showCords = true;
                    //chatHook.mainChat("showing cords", servername, _sender);
                }
                if (_args[1].Equals("rg"))
                {
                    showRegion = true;
                    //chatHook.mainChat("showing cords", servername, _sender);
                }
            }


            p.UpdateHomePosition(p.CurrentPosition);
            int x = (int)p.CurrentPosition.X;
            int y = (int)p.CurrentPosition.Y;
            int z = (int)p.CurrentPosition.Z;


            string eastwest = "";
            string northsouth = "";
            //#x -west/east+   
            if (x < 0)
            {
                eastwest = Math.Abs(x) + " West";
            }
            else
            {
                eastwest = x + " East";
            }
            //#z -South/North+   
            if (z < 0)
            {
                northsouth = Math.Abs(z) + " South";
            }
            else
            {
                northsouth = z + " North";
            }





            p.Message("[4CD2FF]You are [-][BFEFFF]" + northsouth + ", " + eastwest + "[-][FFFFFF] // [-][FF8000]x[-][FFFFFF] " + x + ", [-][FF8000]y[-][FFFFFF] " + y + ", [-][FF8000]z[-][FFFFFF] " + z);

            string regionfile = "";
            if (showRegion)
            {
                int xt = (int)Math.Floor(x / 512.0);
                int zt = (int)Math.Floor(z / 512.0);
                regionfile = "r." + xt + "." + zt + ".7rg";
                p.Message("[FF8000]Region file:[-] " + regionfile);
            }

            Found foundem = new Found();
           try
            {
                if (true)
                {
                        int claimsize = 51;
                        claimsize = claimsize - 1;
                        claimsize = claimsize / 2;
                        string steamidcheck = "";
                        //IReadOnlyList<IPlayer> allPlayers = server.AllPlayers.Players;
                        foreach (var currentPlayer in _allPlayers)
                        {

                            string name = currentPlayer.Name;
                            string steamid = currentPlayer.SteamID;


                           // Console.WriteLine(String.Format(name + " - "));
                            try
                            {
                                if (currentPlayer.LandProtections.Items.Any())
                                {

                                    IReadOnlyList<IAreaDefiniton> Player_LandProtections_Items = currentPlayer.LandProtections.Items;

                                    ClaimBlocks foundClaims = new ClaimBlocks();
                                    //Console.WriteLine(String.Format(name + " LCB found "));

                                    if (Player_LandProtections_Items != null)
                                    {
                                        foreach (var item in Player_LandProtections_Items)
                                        {
                                            int _x = (int)item.Center.X;
                                            int _y = (int)item.Center.Y;
                                            int _z = (int)item.Center.Z;
                                            int checkX = areaCheck(x, _x, claimsize);
                                            int checkY = areaCheck(y, _y, claimsize + 300);
                                            int checkZ = areaCheck(z, _z, claimsize);

                                            //Console.WriteLine(String.Format(name + " - " + "// {0}, {1}, {2}, offset: {3}, {4}, {5}", _x, _y, _z, checkX, checkY, checkZ));

                                            if ((checkX != 111111) && (checkZ != 111111))
                                            {
                                                steamidcheck = steamid;
                                                foundClaims.Add(new ClaimBlock(_x, _y, _z, checkX, checkY, checkZ, steamid, name));
                                                //p.Message(String.Format(name + " - " + "// {0}, {1}, {2}, offset: {3}, {4}, {5}", _x, _y, _z, checkX, checkY, checkZ));
                                            }
                                        }
                                        if (steamidcheck == steamid)
                                        {
                                            FoundClaims asdsad = new FoundClaims(foundClaims, steamid, name);
                                            foundem.Add(asdsad);
                                        }
                                    }
                                }

                            }
                            catch (Exception ep)
                            {
                                Console.WriteLine("error info: " + ep.StackTrace);

                            }
                        }

                    }


                    //Found foundclaims = landClaim(x, y, z, p.SteamID, p, server);

                    if (foundem.Any())
                    {

                        p.Message(" [FFFFFF]------------------[-][FF0080]Claimblocks found![-][FFFFFF]------------------[-]");

                        string _steamidCLaim = "";

                        foreach (FoundClaims blocks in foundem)
                        {
                            _steamidCLaim = blocks._steamid;

                            foreach (ClaimBlock block in blocks._foundClaims)
                            {

                                int x1 = block.x;
                                int y1 = block.z;
                                int x2 = x;
                                int y2 = z;

                                var radians = Math.Atan2((y1 - y2), (x1 - x2));
                                var compassReading = radians * (180 / Math.PI);

                                if (compassReading > 90)
                                {
                                    compassReading = 450 - compassReading;
                                }
                                else
                                {
                                    compassReading = 90 - compassReading;
                                }

                                string compass = kz_cmdWho.getCompassDirection(compassReading);


                                double distance = Math.Sqrt(Math.Pow((block.x - x), 2) + Math.Pow((block.y - y), 2) + Math.Pow((block.z - z), 2));
                                int distanceInt = Convert.ToInt32(distance);
                                string blockOwner = block.name;
                                if (block.name == "") { blockOwner = "<Unknown>"; }
                                //chatHook.mainChat("[4CD2FF]" + blockOwner + "[AA55FF] LCB is [FFFFFF]" + distanceInt + "[AA55FF] from you ([FFD455]" + compass + "[FF7F2A]) [FFFFFF]// [FF8000] x [FFFFFF]" + block.x + " (" + block.fromX + ")[FF8000] y [FFFFFF]" + block.y + " (" + block.fromY + ")[FF8000] z [FFFFFF]" + block.z + " (" + block.fromZ + ")", servername, _sender);
                                string cords = "";
                                if (showCords)
                                {
                                    cords = " [FFFFFF]//[-][FF8000] x [-][FFFFFF]" + block.x + ",[-][FF8000] y [-][FFFFFF]" + block.y + ",[-][FF8000] z [-][FFFFFF]" + block.z + "[-]";
                                }
                                if (showRegion)
                                {
                                    x = (int)Math.Floor(block.x / 512.0);
                                    z = (int)Math.Floor(block.z / 512.0);
                                    regionfile = "r." + x + "." + z + ".7rg";
                                    cords = " [FFFFFF]// " + regionfile + "[-]";
                                }
                                p.Message("[4CD2FF]" + blockOwner + "[-][AA55FF] LCB is [-][FFFFFF]" + (distanceInt - 1) + "[-][AA55FF] blocks, facing '[-][FFFFFF]" + compass + "[-][AA55FF]' from you.[-]" + cords);

                            }
                            if (blocks._name != p.Name)
                            {

                                IPlayer target = null;

                                target = server.AllPlayers.FindPlayerBySteamID(blocks._steamid);

                                if (target != null)
                                {
                                    string onlineTime = " onlineTime: " + (target.Age).ToString() + " minutes";
                                    //TODO: Fix onlinetime to show hours, mins    
                                    onlineTime = "";
                                    //string time = p.LastOnline.ToString ("yyyy-MM-dd HH:mm");
                                    //long ticks = ConvertDateTimeToTicks(p.LastOnline); 
                                    string lastSeen = "";
                                    if (!target.IsOnline)
                                    {
                                        lastSeen = "[-][FF5C26] last seen: [-][FFC926]" + timesince(p.LastLogin) + "[-]";
                                        p.Message("[80FF00]" + blocks._name + lastSeen + onlineTime);
                                    }
                                }
                                else
                                {
                                    p.Message("[80FF00]Error could not find player: " + blocks._name + "[-]");
                                }
                            }
                        }
                        // return true;
                    }
           }
                          catch (Exception ep)
           {
               Console.WriteLine("error2 info: " + ep.StackTrace);

           }
                


           // return false;
        }
        public class ClaimBlock
        {

            private int _x;
            private int _y;
            private int _z;
            private int _fromX;
            private int _fromY;
            private int _fromZ;
            private string _steamid;
            private string _name;

            public ClaimBlock(int x, int y, int z, int fromX, int fromY, int fromZ, string steamid, string name)
            {
                _x = x;
                _y = y;
                _z = z;
                _fromX = fromX;
                _fromY = fromY;
                _fromZ = fromZ;
                _steamid = steamid;
                _name = name;
            }
            public string name
            {
                get { return _name; }
                set { _name = value; }
            }
            public string steamid
            {
                get
                {
                    return _steamid;
                }
                set
                {
                    _steamid = value;
                }
            }
            public int x
            {
                get
                {
                    return _x;
                }
                set
                {
                    _x = value;
                }
            }
            public int y
            {
                get
                {
                    return _y;
                }
                set
                {
                    _y = value;
                }
            }
            public int z
            {
                get
                {
                    return _z;
                }
                set
                {
                    _z = value;
                }
            }
            public int fromX
            {
                get
                {
                    return _fromX;
                }
                set
                {
                    _fromX = value;
                }
            }
            public int fromY
            {
                get
                {
                    return _fromY;
                }
                set
                {
                    _fromY = value;
                }
            }
            public int fromZ
            {
                get
                {
                    return _fromZ;
                }
                set
                {
                    _fromZ = value;
                }
            }
        }

        public class ClaimBlocks : List<ClaimBlock>
        {
        }

        public class FoundClaims
        {

            public ClaimBlocks _foundClaims;
            public string _steamid;
            public string _name;

            public FoundClaims(ClaimBlocks foundClaims, string steamid, string name)
            {
                _foundClaims = foundClaims;
                _steamid = steamid;
                _name = name;
            }
        }

        public class Found : List<FoundClaims>
        { }

        public static int areaCheck(int player, int block, int claimsize)
        {
            if ((player <= block) && (player >= (block - claimsize)))
            {
                int difference = claimsize - (player - (block - claimsize));
                return difference;
            }
            if ((player >= block) && (player <= (block + claimsize)))
            {
                int difference = ((block + claimsize) - player) - claimsize;
                return difference;
            }
            return 111111;
        }

        public static Found landClaim(int playerX, int playerY, int playerZ, string steamidRt, IPlayer p , IServerConnection server)
        {
            Found foundem = new Found();
            if (false){
                
                if (p.LandProtections.Items.Any())
                {
                    //IAreaDefiniton ....bad spelling IAreaDefinition
                    IReadOnlyList<IAreaDefiniton> Player_LandProtections_Items = p.LandProtections.Items;

                    ClaimBlocks foundClaims = new ClaimBlocks();
                    Console.WriteLine(String.Format(p.Name + " LCB found "));

                    if (Player_LandProtections_Items != null)
                    {
                        foreach (var item in Player_LandProtections_Items)
                        {
                            Console.WriteLine(String.Format(p.Name + " x: " + item.Center.X + " y: " + item.Center.Y + " z: " + item.Center.Z));
                        }
                    }
                }
            }

               try
                        {
                if (true)
                {

                    int claimsize = 51;
                    claimsize = claimsize - 1;
                    claimsize = claimsize / 2;
                    string steamidcheck = "";
                    IReadOnlyList<IPlayer> allPlayers = server.AllPlayers.Players;
                        foreach (var currentPlayer in allPlayers)
                    {
                        
                        string name = currentPlayer.Name;
                        string steamid = currentPlayer.SteamID;


                        Console.WriteLine(String.Format(name + " - "));
                     
                        if (currentPlayer.LandProtections.Items.Any())
                        {

                            IReadOnlyList<IAreaDefiniton> Player_LandProtections_Items = currentPlayer.LandProtections.Items;

                            ClaimBlocks foundClaims = new ClaimBlocks();
                            Console.WriteLine(String.Format(name + " LCB found "));

                            if (Player_LandProtections_Items != null)
                            {
                                foreach (var item in Player_LandProtections_Items)
                                {

                                    int _x = (int)item.Center.X;
                                    int _y = (int)item.Center.Y;
                                    int _z = (int)item.Center.Z;
                                    int checkX = areaCheck(playerX, _x, claimsize);
                                    int checkY = areaCheck(playerY, _y, claimsize + 300);
                                    int checkZ = areaCheck(playerZ, _z, claimsize);




                                    if ((checkX != 111111) && (checkZ != 111111))
                                    {
                                        steamidcheck = steamid;
                                        foundClaims.Add(new ClaimBlock(_x, _y, _z, checkX, checkY, checkZ, steamid, name));
                                        //p.Message(String.Format(name + " - " + "// {0}, {1}, {2}, offset: {3}, {4}, {5}", _x, _y, _z, checkX, checkY, checkZ));

                                    }
                                }
                                if (steamidcheck == steamid)
                                {
                                    FoundClaims asdsad = new FoundClaims(foundClaims, steamid, name);
                                    foundem.Add(asdsad);

                                }
                            }
                        }
                                            
                    }              
                }
                        }
               catch (Exception ex)
               {
                   Console.WriteLine(String.Format(ex + " - "));
               }
            return foundem;
           
        }

        public static string timesince(DateTime lastSeen)
        {

            try
            {

                // DateTime startTime = new DateTime(DateTime.Today.Year,DateTime.Today.Month,DateTime.Today.Day,DateTime.Today.Hour,DateTime.Today.Minute,30);

                DateTime TimeNow = DateTime.Now;

                DateTime TimeNowMin = TimeNow.AddSeconds(1);

                //TimeSpan difference = startTime.Subtract(endTime);
                TimeSpan difference = TimeNow.Subtract(lastSeen);

                //Log.Out(lastSeen + " - " + TimeNow);

                //  Log.Out("Time Difference (Minutes): " + difference.Minutes);
                // Log.Out("Time Difference (Seconds): " + difference.Seconds);
                //  Log.Out("Time Difference (Milliseconds): " + difference.Milliseconds);

                String Sdays = "";
                String Shours = "";
                String Sminutes = "";
                String Sseconds = "";
                String Smseconds = "";


                if (difference.Days > 720)
                {
                    return "<Unknown>";
                }

                if (difference.Days > 360)
                {
                    return "OVER A YEAR AGO!";
                }


                if (difference.Days > 1)
                {
                    Sdays = difference.Days + " days, ";
                }
                else if (difference.Days == 1)
                {
                    Sdays = difference.Days + " day, ";
                }
                else
                {
                    Sdays = "";
                }
                if (difference.Hours > 1)
                {
                    Shours = difference.Hours + " hours, ";
                }
                else if (difference.Hours == 1)
                {
                    Shours = difference.Hours + " hour, ";
                }
                else
                {
                    Shours = "";
                }
                if (difference.Minutes > 1)
                {
                    Sminutes = difference.Minutes + " minutes";
                }
                else if (difference.Minutes == 1)
                {
                    Sminutes = difference.Minutes + " minute";
                }
                else
                {
                    Sminutes = "";
                }
                if (difference.Hours == 0 && difference.Seconds > 1)
                {
                    if (difference.Minutes == 0)
                    {
                        Sseconds = difference.Seconds + " seconds";
                    }
                    else
                    {
                        Sseconds = " & " + difference.Seconds + " seconds";
                    }
                }
                else if (difference.Seconds == 1)
                {
                    if (difference.Minutes == 0)
                    {
                        Sseconds = difference.Seconds + " second";
                    }
                    else
                    {
                        Sseconds = " & " + difference.Seconds + " second";
                    }
                }
                if (difference.Milliseconds > 1)
                {
                    Smseconds = difference.Milliseconds + " millseconds";
                }
                else if (difference.Milliseconds == 1)
                {
                    Smseconds = difference.Milliseconds + " millsecond";
                }
                return Sdays + Shours + Sminutes + Sseconds + " ago";

            }
            catch (Exception e)
            {
             Console.WriteLine("Error in Info.timesince: " + e);
            }
            return "-";
        }
    }

}