﻿using _7DTDManager.Interfaces;
using _7DTDManager.Interfaces.Commands;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _7DTDManager.Commands
{
    using bb = _bbcode;

    public class kz_cmdVersion : PublicCommandBase
    {
        public kz_cmdVersion()
        {
            CommandHelp = "Shows Koolio server fixes version info";
            CommandName = "version";
        }

        public override bool Execute(IServerConnection server, IPlayer p, params string[] args)
        {
            if (args.Length == 2 && args[1] == "?") {
                cmdHelp.QuestionHelp(CommandName, p);
                return false;
            }

            bool listAll = false;
                    if (args.Length >= 2)
                    {
                        if (args[1].ToLower() == "all")
                        {
                            listAll = true;
                        }
                    }
                    int count = _Kz_Settings.versionInfo.Length;

                    if (listAll) {
                        for (int i = 0; i < count; i++) {
                            p.Message(_Kz_Settings.versionInfo[i]);
                        }
                    } else {
                        p.Message("Use '/version all' to get full changelog");
                        for (int i = count-7; i < count; i++) {
                            p.Message(_Kz_Settings.versionInfo[i]);
                        }
                        
                    }

            //reversed array
            /*
                    if (listAll) {
                        for (int i = count - 1; i >= 0; i--) {
                            p.Message(_Kz_Settings.versionInfo[i]);
                        }
                    } else {
                        for (int i = count - 1; i >= count-5; i--) {
                            p.Message(_Kz_Settings.versionInfo[i]);
                        }
                        p.Message("Use '/version all' to get full changelog");
                    }
            */
			p.Message(bb._txt2C + "Koolio extensions: [-][FFFFFF] " + _Kz_Settings.KzVersion + bb._cc + bb._txt2C + "// 7DTDmanager [-][FFFFFF]" + System.Reflection.Assembly.GetExecutingAssembly().GetName().Version + bb._cc);

            return true;
        }
    }
}
