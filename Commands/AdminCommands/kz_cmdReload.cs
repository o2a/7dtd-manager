﻿using _7DTDManager.Interfaces;
using _7DTDManager.Interfaces.Commands;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace _7DTDManager.Commands
{
    public class kz_cmdReload : AdminCommandBase
    {
		public kz_cmdReload()
        {
            CommandName = "kreload";
            CommandHelp = "Reload configs.";
            CommandLevel = 100;

            CommandUsage = "/kreload";
        }

        public override bool Execute(IServerConnection server, IPlayer p, params string[] args)
        {
            try
            {
                if (args.Length != 2)
                {
                    Commands.kz_cmdHome.load_homesXML();
                    _Kz_Settings.loadXML();
                    bbcodeReload();    

                    //kzSettings.loadXML();
                    //chatHook.loadedSettings = true;
                    p.Message("All configs reloaded");
                    return true;
                }
                try
                {
                    if (args.Length == 2)
                    {
                        string configType = args[1].ToLower();
                        if (configType == "homes" || configType == "playerhomes.xml")
                        {
                            Commands.kz_cmdHome.load_homesXML();
                            
                            p.Message("Player Homes config reloaded");
                            return true;
                        }
                        else
                        {
                            p.Message("Unable to reload config: " + configType);
                        }
                        if (configType == "settings" || configType == "kzSettings.xml")
                        {
                            _Kz_Settings.loadXML();
                            bbcodeReload();    
                            p.Message("Server Settings config reloaded");
                            return true;
                        }
                        else
                        {
                            p.Message("Unable to Settings config: " + configType);
                        }
                        return true;
                    }
                    else
                    {
                        p.Message("Error in Reload.RunInternal: Failed to reload ");
                    }
                }
                catch (Exception e)
                {
                    // p.Message("Error in Reload.RunInternal: _params.Length == 1 " + args[1] + " invalid " + e);
                    //p.Message("Error in Reload.RunInternal: _params.Length == 1 " + args[1] + " invalid " + e);
                }

            }
            catch (Exception e)
            {
                //Log.Out("Error in Reload.RunInternal: " + e);
            }
            return true;


        }
            public void bbcodeReload() {

                PropertyInfo[] properties  = typeof(_bbcode).GetProperties();

                foreach (var item in properties)
                {
                    object obj = null;
                    Console.WriteLine("Name: " + item.Name + ", Value: " + item.GetValue(obj, null).ToString());
                }
				if (_Kz_Settings.loadedSettings) {
                    _bbcode._servername = _Kz_Settings.Settings.Instance.Server.ServerPrivateChat;
                    _bbcode._servernameAll = _Kz_Settings.Settings.Instance.Server.ServerPublicChat;
                    _bbcode._fsC = _Kz_Settings.Settings.Instance.Colors.ForwardSlash;
					_bbcode._cmdC = _Kz_Settings.Settings.Instance.Colors.Commands;
					_bbcode._argC = _Kz_Settings.Settings.Instance.Colors.Arguements;
					_bbcode._txtC = _Kz_Settings.Settings.Instance.Colors.Text;
					_bbcode._linkC = _Kz_Settings.Settings.Instance.Colors.Links;
                    _bbcode._warningsC = _Kz_Settings.Settings.Instance.Colors.Warnings;
                    _bbcode._errorsC = _Kz_Settings.Settings.Instance.Colors.Errors;
                    _bbcode._cordsC = _Kz_Settings.Settings.Instance.Colors.Cords;

                    _Kz_Settings.defaultSay = _Kz_Settings.Settings.Instance.Server.defaultSay;
                    _Kz_Settings.telnetDelay = _Kz_Settings.Settings.Instance.Server.telnetDelay;
                    _Kz_Settings.regionFolder = _Kz_Settings.Settings.Instance.Server.regionFolder;

					_Kz_Settings.loadedSettings = false;
				}
             }
    }


}



  