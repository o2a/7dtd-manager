﻿using _7DTDManager.Interfaces;
using _7DTDManager.Interfaces.Commands;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace _7DTDManager.Commands {
    public class kz_cmdAscii : AdminCommandBase {
        public kz_cmdAscii() {
            CommandName = "ascii";
            CommandHelp = "Ascii spamm.";
            CommandLevel = 100;

            CommandUsage = "/ascii";
        }

        public override bool Execute(IServerConnection server, IPlayer p, params string[] args) {
            
            p.Message("----------[FF0000]/[-][FF0000]\\[-]----[FF0000]/[-][FF0000]\\[-]----[FF0000]/[-][FF0000]\\[-]----[FF0000]/[-][FF0000]\\[-]----[FF0000]/[-]");
            p.Message("---------[FF0000]/[-]--[FF0000]\\[-]--[FF0000]/[-]--[FF0000]\\[-]--[FF0000]/[-]--[FF0000]\\[-]--[FF0000]/[-]--[FF0000]\\[-]--[FF0000]/[-]-[FF0000]/[-]");
            p.Message("--------[FF0000]/[-]-[FF0000]/[-][FF0000]\\[-]-[FF0000]\\[-][FF0000]/[-]-[FF0000]/[-][FF0000]\\[-]-[FF0000]\\[-][FF0000]/[-]-[FF0000]/[-][FF0000]\\[-]-[FF0000]\\[-][FF0000]/[-]-[FF0000]/[-][FF0000]\\[-]-[FF0000]\\[-][FF0000]/[-]-[FF0000]/[-]");
            p.Message("-------[FF0000]/[-]-[FF0000]/[-]--[FF0000]\\[-]-[FF0000]\\[-][FF0000]/[-]--[FF0000]\\[-]-[FF0000]\\[-][FF0000]/[-]--[FF0000]\\[-]-[FF0000]\\[-][FF0000]/[-]--[FF0000]\\[-]-[FF0000]\\[-][FF0000]/[-]");
            p.Message("-------[FF0000]\\[-]-[FF0000]\\[-]--[FF0000]/[-][FF0000]\\[-]-[FF0000]\\[-]--[FF0000]/[-][FF0000]\\[-]-[FF0000]\\[-]--[FF0000]/[-][FF0000]\\[-]-[FF0000]\\[-]--[FF0000]/[-][FF0000]\\[-]-[FF0000]\\[-]");
            p.Message("--------[FF0000]\\[-]-[FF0000]\\[-][FF0000]/[-]-[FF0000]/[-][FF0000]\\[-]-[FF0000]\\[-][FF0000]/[-]-[FF0000]/[-][FF0000]\\[-]-[FF0000]\\[-][FF0000]/[-]-[FF0000]/[-][FF0000]\\[-]-[FF0000]\\[-][FF0000]/[-]-[FF0000]/[-][FF0000]\\[-]-[FF0000]\\[-]");
            p.Message("---------[FF0000]\\[-][FF0000]/[-]-[FF0000]/[-]--[FF0000]\\[-][FF0000]/[-]-[FF0000]/[-]--[FF0000]\\[-][FF0000]/[-]-[FF0000]/[-]--[FF0000]\\[-][FF0000]/[-]-[FF0000]/[-]--[FF0000]\\[-]-[FF0000]\\[-]");
            p.Message("---------[FF0000]/[-]-[FF0000]/[-][FF0000]\\[-]--[FF0000]/[-]-[FF0000]/[-][FF0000]\\[-]--[FF0000]/[-]-[FF0000]/[-][FF0000]\\[-]--[FF0000]/[-]-[FF0000]/[-][FF0000]\\[-]--[FF0000]/[-]-[FF0000]/[-]");
            p.Message("--------[FF0000]/[-]-[FF0000]/[-][FF0000]\\[-]-[FF0000]\\[-][FF0000]/[-]-[FF0000]/[-][FF0000]\\[-]-[FF0000]\\[-][FF0000]/[-]-[FF0000]/[-][FF0000]\\[-]-[FF0000]\\[-][FF0000]/[-]-[FF0000]/[-][FF0000]\\[-]-[FF0000]\\[-][FF0000]/[-]-[FF0000]/[-]");
            p.Message("-------[FF0000]/[-]-[FF0000]/[-]--[FF0000]\\[-]-[FF0000]\\[-][FF0000]/[-]--[FF0000]\\[-]-[FF0000]\\[-][FF0000]/[-]--[FF0000]\\[-]-[FF0000]\\[-][FF0000]/[-]--[FF0000]\\[-]-[FF0000]\\[-][FF0000]/[-]");
            p.Message("-------[FF0000]\\[-]-[FF0000]\\[-]--[FF0000]/[-][FF0000]\\[-]-[FF0000]\\[-]--[FF0000]/[-][FF0000]\\[-]-[FF0000]\\[-]--[FF0000]/[-][FF0000]\\[-]-[FF0000]\\[-]--[FF0000]/[-][FF0000]\\[-]-[FF0000]\\[-]");
            p.Message("--------[FF0000]\\[-]-[FF0000]\\[-][FF0000]/[-]-[FF0000]/[-][FF0000]\\[-]-[FF0000]\\[-][FF0000]/[-]-[FF0000]/[-][FF0000]\\[-]-[FF0000]\\[-][FF0000]/[-]-[FF0000]/[-][FF0000]\\[-]-[FF0000]\\[-][FF0000]/[-]-[FF0000]/[-][FF0000]\\[-]-[FF0000]\\[-]");
            p.Message("---------[FF0000]\\[-][FF0000]/[-]-[FF0000]/[-]--[FF0000]\\[-][FF0000]/[-]-[FF0000]/[-]--[FF0000]\\[-][FF0000]/[-]-[FF0000]/[-]--[FF0000]\\[-][FF0000]/[-]-[FF0000]/[-]--[FF0000]\\[-]-[FF0000]\\[-]");
            p.Message("---------[FF0000]/[-]-[FF0000]/[-][FF0000]\\[-]--[FF0000]/[-]-[FF0000]/[-][FF0000]\\[-]--[FF0000]/[-]-[FF0000]/[-][FF0000]\\[-]--[FF0000]/[-]-[FF0000]/[-][FF0000]\\[-]--[FF0000]/[-]-[FF0000]/[-]");
            p.Message("--------[FF0000]/[-]-[FF0000]/[-][FF0000]\\[-]-[FF0000]\\[-][FF0000]/[-]-[FF0000]/[-][FF0000]\\[-]-[FF0000]\\[-][FF0000]/[-]-[FF0000]/[-][FF0000]\\[-]-[FF0000]\\[-][FF0000]/[-]-[FF0000]/[-][FF0000]\\[-]-[FF0000]\\[-][FF0000]/[-]-[FF0000]/[-]");
            p.Message("-------[FF0000]/[-]-[FF0000]/[-]--[FF0000]\\[-]-[FF0000]\\[-][FF0000]/[-]--[FF0000]\\[-]-[FF0000]\\[-][FF0000]/[-]--[FF0000]\\[-]-[FF0000]\\[-][FF0000]/[-]--[FF0000]\\[-]-[FF0000]\\[-][FF0000]/[-]");
            p.Message("-------[FF0000]\\[-]-[FF0000]\\[-]--[FF0000]/[-][FF0000]\\[-]-[FF0000]\\[-]--[FF0000]/[-][FF0000]\\[-]-[FF0000]\\[-]--[FF0000]/[-][FF0000]\\[-]-[FF0000]\\[-]--[FF0000]/[-][FF0000]\\[-]-[FF0000]\\[-]");
            p.Message("--------[FF0000]\\[-]-[FF0000]\\[-][FF0000]/[-]-[FF0000]/[-][FF0000]\\[-]-[FF0000]\\[-][FF0000]/[-]-[FF0000]/[-][FF0000]\\[-]-[FF0000]\\[-][FF0000]/[-]-[FF0000]/[-][FF0000]\\[-]-[FF0000]\\[-][FF0000]/[-]-[FF0000]/[-][FF0000]\\[-]-[FF0000]\\[-]");
            p.Message("---------[FF0000]\\[-][FF0000]/[-]-[FF0000]/[-]--[FF0000]\\[-][FF0000]/[-]-[FF0000]/[-]--[FF0000]\\[-][FF0000]/[-]-[FF0000]/[-]--[FF0000]\\[-][FF0000]/[-]-[FF0000]/[-]--[FF0000]\\[-]-[FF0000]\\[-]");
            p.Message("---------[FF0000]/[-]-[FF0000]/[-][FF0000]\\[-]--[FF0000]/[-]-[FF0000]/[-][FF0000]\\[-]--[FF0000]/[-]-[FF0000]/[-][FF0000]\\[-]--[FF0000]/[-]-[FF0000]/[-][FF0000]\\[-]--[FF0000]/[-]-[FF0000]/[-]");
            p.Message("--------[FF0000]/[-]-[FF0000]/[-][FF0000]\\[-]-[FF0000]\\[-][FF0000]/[-]-[FF0000]/[-][FF0000]\\[-]-[FF0000]\\[-][FF0000]/[-]-[FF0000]/[-][FF0000]\\[-]-[FF0000]\\[-][FF0000]/[-]-[FF0000]/[-][FF0000]\\[-]-[FF0000]\\[-][FF0000]/[-]-[FF0000]/[-]");
            p.Message("---------[FF0000]/[-]--[FF0000]\\[-]--[FF0000]/[-]--[FF0000]\\[-]--[FF0000]/[-]--[FF0000]\\[-]--[FF0000]/[-]--[FF0000]\\[-]--[FF0000]/[-]");
            p.Message("--------[FF0000]/[-]----[FF0000]\\[-][FF0000]/[-]----[FF0000]\\[-][FF0000]/[-]----[FF0000]\\[-][FF0000]/[-]----[FF0000]\\[-][FF0000]/[-]-");
            return true;
        }
    }
}