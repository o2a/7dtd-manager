﻿using _7DTDManager.Interfaces;
using _7DTDManager.Interfaces.Commands;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _7DTDManager.Commands
{
    public class kz_cmdSave : AdminCommandBase
    {
		public kz_cmdSave()
        {
            CommandName = "ksave";
            CommandHelp = "Save configs.";
            CommandLevel = 100;
            CommandUsage = "/ksave";
        }

        public override bool Execute(IServerConnection server, IPlayer p, params string[] args)
        {
           try{
                if (args.Length == 1) {
                    Commands.kz_cmdSetHome.save_homeXML();
                    _Kz_Settings.saveXML();
                    p.Message("All configs saved");
                    return true;
                }
                try {
                    if (args.Length > 1) {
                        string configType = args[1].ToLower();

                        switch (configType)
	                    {
	                        case "homes":
                                Commands.kz_cmdSetHome.save_homeXML();
                                p.Message("player Homes config saved");
		                        break;
                            case "playerhomes.xml":
                                Commands.kz_cmdSetHome.save_homeXML();
                                p.Message("player Homes config saved");
                                break;
                            case "config":
                                _Kz_Settings.saveXML("kzSettings_test.xml");
                                break;
                            default:
                                p.Message("Unable to save config: " + configType);
                                break;
                        }
                        return true;
                    } else {
                        p.Message("Error in Save.RunInternal: Failed to reload ");
                    }
                } catch (Exception e) {
                    //Log.Out("Error in Save.RunInternal: _params.Length == 1 " + _params[0] + " invalid " + e);
                    p.Message("Error in Save.RunInternal: _params.Length == 1 " + args[1] + " invalid " + e);
                }

            } catch (Exception e) {
                //Log.Out("Error in Save.RunInternal: " + e);
            }
            return true;
        }
    }
}



