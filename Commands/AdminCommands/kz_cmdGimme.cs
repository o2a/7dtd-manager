﻿using _7DTDManager.Interfaces;
using _7DTDManager.Interfaces.Commands;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _7DTDManager.Commands {
    using bb = _bbcode;
    public class kz_cmdGimme : AdminCommandBase {
        public kz_cmdGimme() {
            CommandName = "gimme";
            CommandHelp = "get items";
            CommandLevel = 100;
            CommandUsage = "/gimme";
        }

        public override bool Execute(IServerConnection server, IPlayer p, params string[] args) {
 
            //Only sends to chat if the command was sent from chat, not console.

            int n = 1;
            if (args.Length >= 2) {
                
                if (args.Length == 3) {
                    if (!int.TryParse(args[2], out n) || n <= 0) {
                        p.Message(bb._errorsC+"Amount is not an integer or not greater than zero."+bb._cc);
                        n = 1;
                    }

                    server.Execute("give " + p.EntityID + " " + args[1].ToLower() + " " + n);

                    //p.Message("give " + p.EntityID + " " + args[1].ToLower() + " " + n);
                }
                System.Random random = new System.Random();


                string[] giveResponses = new string[] {
                                   bb._txt3C+"Look at this guy just taking stuff."+ bb._cc ,
                                    bb._txt3C+"That player is totally cheating."+ bb._cc ,
                                    bb._txt3C+"Someone should stop this guy."+ bb._cc ,
                                    bb._txt3C+"Admin Aboose!! Cheating again!.. "+ bb._cc ,
                                    bb._itemC+args[1]+ bb._txt3C+"..A fine choice. "+ bb._cc ,
                                    bb._itemC+args[1]+ bb._txt3C+"..I've seen better items. "+ bb._cc ,
                                    bb._txt3C+"Stop asking for stuff, it's tiring me."+ bb._cc 
                                    };
                int rndNum = random.Next(0, giveResponses.Length - 1);

                   server.PublicMessage(giveResponses[rndNum]);
                    //p.Message(bb._txtC + "Item(s) droppped" + bb._cc);

            }
            return true;
        }
    }
}



