﻿using _7DTDManager.Interfaces;
using _7DTDManager.Interfaces.Commands;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Ionic.Zip;
/* Kz_7dtd _Backup (v0.5)
 * /backup - Copy regions with touching LCBs, into the folder '/Backup', located in the same root as the 'map' folder (smallest backup) 
 * /backup <timestamp|ts> - Same as above, only the files are copied into a new timestamped folder within the '/Backup' folder.
 * /backup <large|lg> - Every region surrounding the region with the LCB is copied into the '/Backup' folder.
 * /backup <large|lg> <timestamp|ts> - Same as above, only the files are copied into a new timestamped folder within the '/Backup' folder.
 * 
 * Notes: The large backup option was really intended to keep regions surrounding an LCB area so there is less map regeneration, after wiping of all map regions.
 * Possible todo features: 
 * - Export list of regions with LCB owner steamid/playernames for the region file(s)
 * - Worldborder
 *    - Copies all regions within a specified worldborder.
 *    - Copies all regions within the worldborder, plus any regions outside the border that have LCB's.
 * 
*/
namespace _7DTDManager.Commands {
    public class kz_cmdBackup : AdminCommandBase {
        public kz_cmdBackup() {
            CommandName = "backup";
            CommandHelp = "Backup files, '/backup ts', timestampped only regions with lcb, or '/backup large' for every region surrounding a lcb. '/backup large ts' works aswel.";
            CommandLevel = 100;

            CommandUsage = "/backup <ts|timestamp|lg|large> <ts|timestamp>";
        }

        public override bool Execute(IServerConnection server, IPlayer p, params string[] args) {
            int backupType = 0;
            //0 Copies regions to Backup folder overwritting each time. (Does not remove existing regions in the folder)
            //1 Copies regions to timestampped folder within the Backup folder.

            int backupSize = 0;
            //0 Large backup, every region surrounding the region with the LCB is backed up.
            //1 Accurate region backup, only the regions touching LCB's are backed up. 
            //TODO: Backup improvements
            //2 Copies all regions within a specified worldborder.
            //3 Copies all regions within the worldborder, plus any regions outside the border that have LCB's.

            if (args.Length == 2) {
                if (args[1].ToLower().Equals("large") || args[1].ToLower().Equals("lg")) {
                    backupSize = 1;
                }
                if (args[1].ToLower().Equals("timestamp") || args[1].ToLower().Equals("ts")) {
                    backupType = 1;
                }
                if (args[1].ToLower().Equals("info")) {
                    server.PrivateMessage(p, "Backup starting...");
                    p.Message("RegionFolder: "+_Kz_Settings.regionFolder);
                    p.Message("BackupFolder: " + _Kz_Settings.backupFolder);
                }
            }
            if (args.Length == 3) {
                if (args[2].ToLower().Equals("timestamp") || args[2].ToLower().Equals("ts")) {
                    backupType = 1;
                    //TODO Possible 2nd arg with timestamp format.
                }
            }

            server.Execute("saveworld");
            server.Execute("llp");

            server.PrivateMessage(p, "Backup starting...");
            Thread t = new Thread(() => {
                Thread.Sleep(5000);
                RunBackup(backupType, backupSize, p, server);
            });
            t.IsBackground = true;
            t.Start();

            return true;
        }
        public static void RunBackup(int backupType, int backupSize, IPlayer p, IServerConnection server) {

            server.PrivateMessage(p, "Backup started...");
            // Create new stopwatch
            System.Diagnostics.Stopwatch stopwatch = new System.Diagnostics.Stopwatch();

            // Begin timing
            stopwatch.Start();

            string timenow = string.Empty;

            string regionFolder = _Kz_Settings.regionFolder;
            string backupFolder = _Kz_Settings.backupFolder;


            string[] Files = new string[] {"AllocsPeristentData.bin",
                "main.ttw",
                "players.xml",
          ".."+Path.DirectorySeparatorChar+".."+Path.DirectorySeparatorChar+"config.xml",
            ".."+Path.DirectorySeparatorChar+".."+Path.DirectorySeparatorChar+"admins.xml",
            ".."+Path.DirectorySeparatorChar+".."+Path.DirectorySeparatorChar+"profiles.sdf",
            ".."+Path.DirectorySeparatorChar+".."+Path.DirectorySeparatorChar+"players.xml"};


            if (System.IO.Directory.Exists(regionFolder) && System.IO.Directory.Exists(backupFolder)) {

                try {

                    List<string> list = backupList(backupSize, server);

                    //string backupPath = ".." + Path.DirectorySeparatorChar + ".." + Path.DirectorySeparatorChar + ".." + Path.DirectorySeparatorChar + "" + regionFolder + Path.DirectorySeparatorChar + "Backup" + Path.DirectorySeparatorChar;

                    //backupFolder = "/home/sdtd/instances/Backup";

                    //Directory.CreateDirectory(regionFolder + Path.DirectorySeparatorChar + "Backup");

                    if (backupType == 1) {
                        timenow = DateTime.Now.ToString("yyyy-MM-dd_hh-mm-ss-tt");
                        Directory.CreateDirectory(backupFolder + Path.DirectorySeparatorChar + timenow);
                    }
                    //string regionPath = regionFolder + Path.DirectorySeparatorChar + "Region" + Path.DirectorySeparatorChar;

                    //string path = GamePrefs.GetString(EnumGamePrefs.SaveGameFolder) + Path.DirectorySeparatorChar + GamePrefs.GetString(EnumGamePrefs.GameWorld) + Path.DirectorySeparatorChar + GamePrefs.GetString(EnumGamePrefs.GameName) + Path.DirectorySeparatorChar + "Region" + Path.DirectorySeparatorChar;
                    if (list != null) {

                        //string regionBackup = "regionBackup/";
                        //regionBackup = "";
                        int copyCounter = 0;
                        try {
                            //Directory.CreateDirectory(GamePrefs.GetString(EnumGamePrefs.SaveGameFolder) + "/regionBackup");
                            // TextWriter textWriter = new StreamWriter(GamePrefs.GetString(EnumGamePrefs.SaveGameFolder) + Path.DirectorySeparatorChar + regionBackup + "regionBackup" + ".xml");
                            // textWriter.WriteLine("=Copied the following region filenames, total count: " + (list.Count).ToString() + "=");
                            //textWriter.WriteLine("=================================================");
                            foreach (string region in list) {
                                if (File.Exists(regionFolder + region)) {
                                    Console.WriteLine(regionFolder + region);
                                    File.Copy(regionFolder + region, backupFolder + ((backupType == 1) ? timenow + Path.DirectorySeparatorChar : "") + region, true);
                                    copyCounter++;
                                }
                                //textWriter.WriteLine(StaticDirectories.GetSaveGameDir() + Path.DirectorySeparatorChar + "Backup" + Path.DirectorySeparatorChar + region);
                            }

                            Console.WriteLine(copyCounter + " region files copied to the folder: " + backupFolder + ((backupType == 1) ? timenow + Path.DirectorySeparatorChar : ""));
                            p.Message(copyCounter + " region files copied to the backup folder");
                            // Write result
                            p.Message("Time elapsed: {0}", stopwatch.Elapsed);
                            //return true;
                        } catch (Exception e) {
                            Console.WriteLine("Error in Backup.Run.saving: " + e);
                        }
                    } else {
                        Console.WriteLine("Error in Backup.Run Region list returned null");
                    }

                } catch (Exception e) {
                    Console.WriteLine("Error in Backup.Run: " + e);
                }

            } else {
                p.Message("Backup aborted, the folder to regionfiles: '" + regionFolder+"' or backupfolder does not exist: '"+backupFolder+"'");
            }
            // Stop timing
            stopwatch.Stop();

        }

        public static List<string> backupList(int backupSize, IServerConnection server) {

            List<string> regions = new List<string>();

            int claimsize = 51;
            claimsize = claimsize - 1;
            claimsize = claimsize / 2;

            var allPlayers = server.AllPlayers.Players;
            foreach (var currentPlayer in allPlayers) {
                string name = currentPlayer.Name;
                string steamid = currentPlayer.SteamID;


                Console.WriteLine(String.Format(name + " - "));

                if (currentPlayer.LandProtections.Items.Any()) {


                    IReadOnlyList<IAreaDefiniton> Player_LandProtections_Items = currentPlayer.LandProtections.Items;

                    Console.WriteLine(String.Format(name + " LCB found "));

                    if (Player_LandProtections_Items != null) {
                        foreach (var item in Player_LandProtections_Items) {


                            int _x = (int)item.Center.X;
                            int _y = (int)item.Center.Y;
                            int _z = (int)item.Center.Z;

                            Console.WriteLine(name + ": " + _x + ", " + _y + ", " + _z);

                            /* 
                               x = -west/east+   
                               z = -South/North+   
                               c = claimblockpoint, t=top, b=bottom, l=left, r=right
                                ctl----------ctr
                                 |            |
                                 |            |
                                 |     xz 0   |
                                 |            |
                                 |            |
                                cbl----------cbr
                              */
                            //bool addit = true;

                            if (backupSize == 0) {

                                int[] _ctl = { _x - claimsize, _y, _z + claimsize };
                                int[] _ctr = { _x + claimsize, _y, _z + claimsize };
                                int[] _cbl = { _x - claimsize, _y, _z - claimsize };
                                int[] _cbr = { _x + claimsize, _y, _z - claimsize };

                                int[][] claimPoints = { _ctl, _ctr, _cbl, _cbr };

                                List<string> regionPoints = new List<string>();

                                foreach (int[] claimPoint in claimPoints) {
                                    int xPoint = claimPoint[0];
                                    //int yPoint = claimPoint[1];
                                    int zPoint = claimPoint[2];

                                    int x = (int)Math.Floor((double)xPoint / 512);
                                    int z = (int)Math.Floor((double)zPoint / 512);

                                    string regionName = "r." + x.ToString() + "." + z.ToString() + ".7rg";

                                    Console.WriteLine(name + " addedTo (regionPoints): " + regionName + "// x" + xPoint + ", z:" + zPoint);

                                    regions.Add(regionName);
                                    /*
                                    //Adds region filenames, and check for existing region filenames to prevent duplicates.
                                    addit = true;
                                    foreach (string region in regionPoints)//1,2,4,5
                                    {
                                        //if (regionName == region)
                                        if (regionName.Equals(region))//1
                                        {
                                            addit = false;
                                            //break;
                                        }
                                    }
                                    if (addit)
                                    {
                                        Console.WriteLine(name + " addedTo (regionPoints): " + regionName + "// x" + xPoint + ", z:" + zPoint);
                                        regionPoints.Add(regionName);
                                    }
                                    */
                                }

                                /*

                                addit = true;
                                foreach (string regioncheck in regionPoints)
                                {
                                    foreach (string region in regions)
                                    {
                                        if (regioncheck.Equals(region))
                                        {
                                            Console.WriteLine(regioncheck + " == " + region + " ..found so " + regioncheck + "not added");
                                            addit = false;
                                            //break;
                                        }
                                        else {

                                            addit = true;
                                        }
                                        //Console.WriteLine("regioncheck == region cancelled:" + name + " added (regionPoints): " + regioncheck);

                                    }
                                    if (addit)
                                    {

                                        Console.WriteLine(name + " addedTo (regions): " + regioncheck);
                                        regions.Add(regioncheck);
                                    }
                                }
                                */
                            } else {
                                int x = (int)Math.Floor((double)_x / 512);
                                int z = (int)Math.Floor((double)_z / 512);

                                string[] addtoRegions = {"r." + x.ToString() + "." + z.ToString() + ".7rg",
                                                "r." + (x+1).ToString() + "." + (z+1).ToString() + ".7rg",
                                                "r." + (x+1).ToString() + "." + (z+1).ToString() + ".7rg",
                                                "r." + (x+1).ToString() + "." + (z).ToString() + ".7rg", 
                                                "r." + (x+1).ToString() + "." + (z-1).ToString() + ".7rg", 
                                                "r." + (x-1).ToString() + "." + (z+1).ToString() + ".7rg", 
                                                "r." + (x-1).ToString() + "." + (z).ToString() + ".7rg", 
                                                "r." + (x-1).ToString() + "." + (z-1).ToString() + ".7rg", 
                                                "r." + (x).ToString() + "." + (z+1).ToString() + ".7rg", 
                                               };

                                //addit = true;
                                foreach (string regioncheck in addtoRegions) {
                                    /*
                                    foreach (string region in regions)
                                    {
                                        if (regioncheck.Equals(region))
                                        {
                                            addit = false;
                                            //break;
                                        }

                                    }
                                    if (addit)
                                    {
                                        regions.Add(regioncheck);
                                    }
                                    */
                                    regions.Add(regioncheck);

                                }


                            }

                        }
                    }
                }
            }
            /*
            using (System.IO.StreamWriter file = new System.IO.StreamWriter("backupregions_main.txt"))
            {
                int i = 0;
                foreach (string line in regions)
                {
                    i++;
                    // If the line doesn't contain the word 'Second', write the line to the file. 
                    //if (!line.Contains("Second"))
                    // {
                    //     file.WriteLine(line);
                    // }
                    
                    file.WriteLine(line);
                }
                file.WriteLine("Total files: " + i);
            }
            */

            //regions = regions.Distinct().ToList();

            //Console.WriteLine(String.Join(", ", list)); // prints a, b, A, c, d

            // In case you want to use case insensitive, invariant culture comparison, use this one. Different comparers are available: ////http://msdn.microsoft.com/en-us/library/system.stringcomparer(v=vs.110).aspx
            regions = regions.Distinct(StringComparer.InvariantCultureIgnoreCase).ToList();

            /*
             using (System.IO.StreamWriter file = new System.IO.StreamWriter("backupregions.txt"))
             {
                 int i = 0;
                 foreach (string line in regions)
                 {
                     i++;
                     // If the line doesn't contain the word 'Second', write the line to the file. 
                     if (!line.Contains("Second"))
                     {
                         file.WriteLine(line);
                     }
                    
                     file.WriteLine(line);
                 }
                 file.WriteLine("Total files: "+i);
             }
  */

            return regions;

        }
    }
}



