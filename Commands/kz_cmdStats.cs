﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Text.RegularExpressions;
using System.Xml.Serialization;
using _7DTDManager.Interfaces;
using _7DTDManager.Interfaces.Commands;
using System.Threading;
using _7DTDManager.Objects;
using NLog;

namespace _7DTDManager.Commands
{
    
    using bb = _bbcode;
    using MySql.Data.MySqlClient;

    public class kz_cmdStats : PublicCommandBase {
        private static Logger logger = LogManager.GetCurrentClassLogger();

        public kz_cmdStats()
        {
            CommandCost = 10;
            //CommandCoolDown = 0;
            CommandHelp = "Shows your server stats or another players/stats <name|id>";
            CommandName = "kstats";
        }

        public override bool Execute(IServerConnection server, IPlayer p, params string[] args)
        {
            if (args.Length == 2 && args[1] == "?") {
                cmdHelp.QuestionHelp(CommandName, p);
                return false;
            }
            try
            {
                IPlayer target = null;
                if (args.Length > 1)
                {
                    target = server.AllPlayers.FindPlayerByNameOrID(args[1]);
                    if ((target == null) || (!target.IsOnline))
                    {
                        //p.Message("Targetplayer '{0}' was not found or is not online.", args[1]);
                        p.Message(bb._orange + "Stats error: playername or entity ID '" + _bbcode._cc + "[FFFFFF]" + args[1] + "[-]" + bb._orange + "' not found" + _bbcode._cc);
                        return false;
                    }
                }
                else
                {
                    target = p;
                }

                if (true)
                {
                    int P_AdminLevel = target.AdminLevel;
                    int P_Age = target.Age;
                    int P_BloodCoins = target.BloodCoins;
                    int P_Bounty = target.Bounty;
                    int P_BountyCollected = target.BountyCollected;
                    double P_CurrentPosition_X = target.CurrentPosition.X;
                    double P_CurrentPosition_Y = target.CurrentPosition.Y;
                    double P_CurrentPosition_Z = target.CurrentPosition.Z;
                    int P_Deaths = target.Deaths;
                    double P_DistanceTravelled = target.DistanceTravelled;
                    string P_EntityID = target.EntityID;
                    DateTime P_FirstLogin = target.FirstLogin;
                    /*
                    //IReadOnlyList<IPlayer> P_Friends_Items = target.Friends.Items;
                    string P_FriendsWith = "Friends with ";
                    foreach (var friend in target.Friends.Items)
                    {
                        string friendName = friend.Name;
                        if (p.IsAdmin)
                        {
                            P_FriendsWith += friendName + ", ";
                            //p.Message("Friends with " + friendName);
                        }
                    }
                    */
                    string P_IPAddress = target.IPAddress;
                    bool P_IsAdmin = target.IsAdmin;
                    bool P_IsOnline = target.IsOnline;
                    //IReadOnlyList<IPosition> P_LandProtections_Items = target.LandProtections.Items;
                    //string[] P_LandProtections_Items = {};
                    int i = 0;

                    if (target.LandProtections.Items != null)
                    {
                        foreach (var lcb in target.LandProtections.Items)
                        {
                            i++;
                            int x = (int)lcb.Center.X;
                            int y = (int)lcb.Center.Y;
                            int z = (int)lcb.Center.Z;
                            if (p.IsAdmin)
                            {
                               // P_LandProtections_Items[i - 1] = bb._txt3C + "LCB: " + bb._cc + bb._cordsC + "x: " + bb._cc + x + bb._cordsC + ", y: " + bb._cc + y + bb._cordsC + ", z: " + bb._cc + z;
                                //p.Message("Friends with " + friendName);
                            }
                        }
                    }
                    string totalLCB = bb._txt3C+"Total LCB: " +bb._cc + i.ToString();;

                    DateTime P_LastLogin = target.LastLogin;
                    DateTime P_LastPayday = target.LastPayday;
                    IReadOnlyList<IMailMessage> P_Mailbox_Mails = target.Mailbox.Mails;
                    string P_Name = target.Name;
                    int P_Ping = target.Ping;
                    int P_PlayerKills = target.PlayerKills;
                    int P_Spent = target.Spent;
                    string P_SteamID = target.SteamID;
                    int P_zCoins = target.zCoins;
                    int P_ZombieKills = target.ZombieKills;

                    p.Message("[FF2A55]{0}" + bb._cc + bb._txt3C + ", ID: " + bb._cc + "{1}" + bb._txt3C + ", Coins: " + bb._cc + "{2}" + bb._txt3C + ", Coins spent: " + bb._cc + "{3}", P_Name, P_EntityID, P_zCoins, P_Spent);
                    p.Message(bb._txt3C + "FirstLogin: " + bb._cc + "{0}" + bb._cc, P_FirstLogin);
                    p.Message(bb._txt3C + "Playtime: " + bb._cc + " {0} minutes", P_Age);
                    p.Message(bb._txt3C + "LastLogin: " +bb._cc+" {0} ", P_LastLogin);
                    p.Message(bb._txt3C + "PlayerKills: " + bb._cc + "{0}" + bb._txt3C + ", ZombieKills: " + bb._cc + "{1}" + bb._txt3C + ", Deaths: " + bb._cc + "{2} ", P_PlayerKills, P_ZombieKills, P_Deaths);
                    p.Message(bb._txt3C + "BloodCoins: " + bb._cc + "{0}" + bb._txt3C + ", Bounty: " + bb._cc + "{1}" + bb._txt3C + ", BountyCollected: " + bb._cc + "{2} ", P_BloodCoins, P_Bounty, P_BountyCollected);
                    

                    //p.Message(P_FriendsWith);  //Not yet implemented
                    p.Message(totalLCB);
                    /*
                    foreach (var item in P_LandProtections_Items)
                    {
                        if (p.IsAdmin)
                        {
                            p.Message(item);
                        }
                    }
                    */

                    try {
                        using (var con = new MySqlConnection(_Mysql.localDB)) {
                            using (var cmd = con.CreateCommand()) {
                                con.Open();

                                System.Data.IDbCommand dbcmd = con.CreateCommand();

                                string totalVotes = "SELECT `username`,`steamid`, COUNT(steamid) " +
                            "FROM `votes` " +
                            "WHERE `steamid` = " + target.SteamID + " " +
                            "GROUP BY `steamid` " +
                            "ORDER BY `COUNT(steamid)` DESC";

                                dbcmd.CommandText = totalVotes;
                                System.Data.IDataReader reader = dbcmd.ExecuteReader();
                                int Votes = 0;
                                while (reader.Read()) {
                                    //string username = (string)reader["username"];
                                    //string steamid = (string)reader["steamid"];
                                    Votes = int.Parse(reader["COUNT(steamid)"].ToString());
                                }

                                //Console.WriteLine("You ranked up, with " + Votes + " votes");
                                //p.Message("You ranked up, with " + Votes + " votes");
                                target.Message(bb._txt3C + "Total server votes: [-][FFFFFF]" + Votes + bb._cc);

                            }
                        }
                    }catch(Exception e){
                        target.Message(bb._txt3C + "Total server votes: [-][FFFFFF] -Try again later Db error" +  bb._cc);
                        logger.Error("Error cmdStats: " + e.Message);
                    }

                    if (p.IsAdmin)
                    {
                        p.Message(bb._txt3C + "Ip Address: "+bb._cc+"{0}"+bb._txt3C + ", SteamID ID: "+bb._cc+"{1} ", P_IPAddress, P_SteamID);
                    }
                }
                else
                {
                    p.Message(bb._warningsC + "You will be able to use this cmd later '/stats' or '/stats <player|id>'" + bb._cc);
                }
            }catch(Exception er){
                Console.WriteLine(bb._warningsC + "error " + er + bb._cc);
                if (p.IsAdmin)
                {
                    p.Message(bb._warningsC + "error "+ er + bb._cc);
                }
                
            }
            return false;
        }

    }
}

