﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Text.RegularExpressions;
using System.Xml.Serialization;
using _7DTDManager.Interfaces;
using _7DTDManager.Interfaces.Commands;
using System.Threading;
using _7DTDManager.Objects;

namespace _7DTDManager.Commands
{
    using bb = _bbcode;
    public class kz_cmdTeleport : PublicCommandBase
    {
        public kz_cmdTeleport()
        {
            CommandCost = 10;
            //CommandCoolDown = 0;
            CommandHelp = bb._txt2C + "Teleport Usage: [-][FFFFFF]/tp" + bb._txt2C + "|" + bb._cc + "[FFFFFF]tele <playername or id> [-]" + bb._txt2C + "Use [-][FFFFFF]/idlist[-]" + bb._txt2C + " to get numerical id of a player" + bb._cc;
            CommandName = "tp";
            CommandAliases =  new string[]{"teleport","tele"};
        }

        public override bool Execute(IServerConnection server, IPlayer p, params string[] args){


            if (args.Length == 2 && args[1] == "?") {
                cmdHelp.QuestionHelp(CommandName, p);
                return false;
            }

            try
            {
            
                if (args.Length < 2)
                {
                    p.Message(bb._txt2C + "Teleport Usage: [-][FFFFFF]/tp" + bb._txt2C + "|" + bb._cc + "[FFFFFF]tele <playername or id> [-]" + bb._txt2C + "Use [-][FFFFFF]/idlist[-]" + bb._txt2C + " to get numerical id of a player" + bb._cc);
                    return false;
                }

                if (args.Length >= 2)
                {
                  
                    p.UpdateHomePosition(p.CurrentPosition);
                    IPlayer target = null;

                    target = server.AllPlayers.FindPlayerByNameOrID(args[1]);

                    if ((target == null) || (!target.IsOnline))
                    {


                        Dictionary<string, double> resultDictionary = new Dictionary<string, double>();
                        foreach (var player in server.AllPlayers.Players) {
                            if (player.IsOnline) {
                                var result = _Kz_Settings.LongestCommonSubsequence(args[1], player.Name);
                                resultDictionary.Add(player.Name, result.Item2);
                            }
                            
                        }
                        string name = string.Empty;
                        double max = 0;
                        //p.Message("Name matching test...");
                        foreach (var kvp in resultDictionary) {

                            //p.Message(kvp.Value + " = " + kvp.Key);
                            if (kvp.Value > max) {
                                max = kvp.Value;
                                name = kvp.Key;
                            }
                        }
                        //p.Message("Best match: "+max + " = " + name);
                        target = server.AllPlayers.FindPlayerByNameOrID(name);

                        if ((target == null) || (!target.IsOnline)) {
                            p.Message(bb._orange + "Teleport error: playername or entity ID '" + _bbcode._cc + "[FFFFFF]" + args[1] + "[-]" + bb._orange + "' not found" + _bbcode._cc);

                            return false;
                        }
                    }
                                                                    
                        if (p.SteamID == "76561197968560745" || p.SteamID == "76561198132011027")
                       // if (p.SteamID == "7656119796856074500" || p.SteamID == "7656119813201102700")
                        {
                            int teleX = (int)target.CurrentPosition.X;
                            int teleY = (int)target.CurrentPosition.Y;
                            int teleZ = (int)target.CurrentPosition.Z;

                            telePort(teleX, teleY , teleZ, target.Name, p.Name, p, server);
                            //chatHook.mainChat("[26C9FF] Teleported " + playerName + " to " + _params[0], servername);
                            return true;
                        }
                        else
                        {
                            string steamidTpto = target.SteamID;
                            //if (steamidTpto == "76561197968560745") {
                            if (steamidTpto == "0000")
                            {
                            int teleX = (int)target.CurrentPosition.X;
                            int teleY = (int)target.CurrentPosition.Y;
                            int teleZ = (int)target.CurrentPosition.Z;

                            telePort(teleX, teleY, teleZ, target.Name, p.Name, p, server);

                                p.Message("[FF2A55] You are allowed to tp to the admin (best to ask first), just don't do anything stupid[-]");
                                return true;
                            }
                            else
                            {
                                //remove a players previous tp request
                                bool duplicateTP = false;
                                foreach (kz_cmdTeleport.telePortQueue queue in kz_cmdTeleport.telePortList.list)
                                {
                                    if (queue.steamidFrom == p.SteamID && queue.steamidTo == steamidTpto)
                                    {
                                        kz_cmdTeleport.telePortList.list.Remove(queue);
                                        TimeSpan difference = DateTime.Now - queue.timeFrom;
                                        double seconds = difference.Seconds;
                                        if (seconds > 0 && seconds <= 15)
                                        {
                                            duplicateTP = true;
                                        }
                                    }
                                }
                                if (duplicateTP)
                                {
                                    //TODO: Prevent tp spamming
                                    p.Message(target.Name + "[7FFF55] has already been notified of your teleport request [-][FFAA2A](Don't spam teleport requests)" + _bbcode._cc);
                                    telePortQueue request = new telePortQueue(steamidTpto, target.Name, p.SteamID, p.Name, DateTime.Now);
                                    telePortList.list.Add(request);
                                    return true;
                                }
                                else
                                {
                                    target.Message(p.Name + bb._txt2C + " has requested to teleport to you. Type [-]" + bb._cmdC + "/tpaccept[-]" + bb._txt2C + " or [-]" + bb._cmdC + "/tpa[-]" + bb._txt2C + " in the chat or in console without the [-]"+bb._white+"/[-]" + bb._txt2C + " to allow teleport to you." + _bbcode._cc);
                                    p.Message(target.Name + "[-]" + bb._purple + " has been notified of your teleport request [-]" + bb._lightOrange + "(wait 15seconds & please don't spam requests)[-]");
                                    telePortQueue request = new telePortQueue(steamidTpto, target.Name, p.SteamID, p.Name, DateTime.Now);
                                    telePortList.list.Add(request);
                                    return true;
                                }
                            }

                            //chatHook.mainChat("[26C9FF] Sorry no permission to use this command, until /tpaccept is implemented", servername,_sender);

                        }
                    
                }
            }
            catch (Exception e)
            {
                
               // logger.Info("4Error in Teleport.RunInternal: " + e);
            }



            return false;
        }



        public static void telePort(int x, int y, int z, string playerTo, string playerFrom, IPlayer p ,IServerConnection server)
        {
            Thread t = new Thread(() => {
                server.Execute("tele {0} {1}", p.EntityID, x + " " + (y + 100) + " " + z);
                // HACK: Teleport fix?
                Thread.Sleep(2500);

                server.Execute("tele {0} {1}", p.EntityID, x + " " + (y + 1) + " " + z);

                //server.Execute("give "+p.EntityID+" splint 1");

                server.PublicMessage(bb._yellow + playerFrom + _bbcode._cc + bb._lightOrange + " teleported to " + _bbcode._cc + bb._yellow + playerTo + _bbcode._cc);
            });
            t.IsBackground = true;
            t.Start();
        }

        public class telePortQueue
        {

            private string _steamidTo;
            private string _nameTo;
            private string _steamidFrom;
            private string _nameFrom;
            private DateTime _timeFrom;

            public telePortQueue(string steamidTo, string nameTo, string steamidFrom, string nameFrom, DateTime timeFrom)
            {
                _steamidTo = steamidTo;
                _nameTo = nameTo;
                _steamidFrom = steamidFrom;
                _nameFrom = nameFrom;
                _timeFrom = timeFrom;

            }

            public string steamidTo
            {
                get { return _steamidTo; }
                set { _steamidTo = value; }
            }

            public string nameTo
            {
                get { return _nameTo; }
                set { _nameTo = value; }
            }
            public string steamidFrom
            {
                get { return _steamidFrom; }
                set { _steamidFrom = value; }
            }
            public string nameFrom
            {
                get { return _nameFrom; }
                set { _nameFrom = value; }
            }
            public DateTime timeFrom
            {
                get { return _timeFrom; }
                set { _timeFrom = value; }
            }


        }

        public class telePortList
        {
            public static List<telePortQueue> list = new List<telePortQueue>();
        }
    }
}

