﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Text.RegularExpressions;
using System.Xml.Serialization;
using _7DTDManager.Interfaces;
using _7DTDManager.Interfaces.Commands;
using System.Threading;
using _7DTDManager.Objects;

namespace _7DTDManager.Commands {
    using bb = _bbcode;
    using NLog;

    public class kz_cmdWarpsV2 : PublicCommandBase {
        public kz_cmdWarpsV2() {
            CommandCost = 10;
            //CommandCoolDown = 0;
            CommandHelp = "Warp to another area /warp for list and /warp <name>, to teleport ";
            CommandName = "warp";
        }
        public static string _warpName = "";
        private static Logger logger = LogManager.GetCurrentClassLogger();

        //public enum argsE {
        //    create,
        //    list,
        //    help,
        //    delete,
        //    edit,
        //    unknown
        //}
        int warpsFound;
        public static string[] argsReserved = new string[] { "create", "delete", "help", "edit", "list" };
        public override bool Execute(IServerConnection server, IPlayer p, params string[] args) {
            string bbclose = "[-]";




            if (args.Length < 2) {
                p.Message(bb._cmdC + "/warp <name>[-]" + bb._txtC + " to teleport to that location (without the <>).[-]");
                warpsFound = 0;
                string[] listwarps = ListWarps(p.SteamID, p.Name, true, 0, out warpsFound);
                if (warpsFound > 0) {
                    foreach (string print in listwarps) {
                        //m_Console.SendResult(print);
                        p.Message(print);
                    }
                } else {
                    p.Message(bb._warningsC+"No public warps found.[-]");
                }
                
                if (p.IsAdmin) {
                    p.Message("[FF2A55]/[-][FF7F2A]warp create <warpname> <group=private|all>[-]");
                    p.Message("[FF2A55]/[-][FF7F2A]warp delete <warpname>[-]");
                    p.Message("[FF2A55]/[-][FF7F2A]warp edit <warpname> <group=private|all> <update cords=yes|no> <enable=yes|no>[-]");
                    
                }
                /*
                p.Message("[26C9FF] Use /warp list: [-][FFFFFF]/warp craptown [-][00B9F2]| [-][FFFFFF]forest [00B9F2]| [-][FFFFFF]desert" + bb._cc);
                p.Message("[26C9FF] Warps: [-][FFFFFF]/warp grasslands [-][00B9F2]| [-][FFFFFF]mainshitty [-][00B9F2]| [-][FFFFFF]plains" + bb._cc);
                 */
                return false;
            }



            string warpName = "default";
            
            int warpFound = 0;  //0 notfound, 1found but ur not allowed, 2 found and you can use it.

            if (args.Length >= 2) {

                //    argsE orderEnum = argsE.unknown;
                //    Enum.TryParse<argsE>(args[1].ToLower(), out orderEnum);
                //switch (orderEnum) {
                //        case argsE.create:

                switch (args[1].ToLower()) {
                    case "create":
                        if (p.IsAdmin) {
                            if (args.Length >= 3) {
                                warpName = args[2].ToLower();

                            string groupName = "all";
                            if (args.Length == 4) {
                                groupName = args[3].ToLower();
                                if (groupName != "all" && groupName != "private") {
                                    p.Message(bb._warningsC + "Invalid group name '" + groupName + "' only 'private' (you and friends) or 'all' (public warp)");
                                    return false;
                                }
                            }
                                bool enabled = true;
                                //string message = "";

                                server.Execute("lp");
                                Thread t = new Thread(() => {
                                    Thread.Sleep(800); //1 second delay
                                    int x = (int)p.CurrentPosition.X;
                                    int y = (int)p.CurrentPosition.Y;
                                    int z = (int)p.CurrentPosition.Z;
                                    Save(p, p.SteamID, p.Name, x, y, z, warpName, groupName, enabled);
                                });
                                t.IsBackground = true;
                                t.Start();
                                return true;

                            } else {
                                p.Message("[FF2A55]/[-][FF7F2A]warp create <warpname> <group=private|all>[-]");
                            }
                        } else {
                            p.Message(bb._warningsC + "This command not available to you, creating private warps will be allowed later for donators and voters who have used points to unlock private warps (private warps can be be used by those on your friendlist)[-]");
                        }
                        return true;
                    case "pcreate":
                        p.Message(bb._txtC + "to be added: [-]");
                        return true;
                    case "delete":
                        if (args.Length == 3) {
                            warpName = args[2].ToLower();
                            if (p.IsAdmin) {

                                p.Message(DeleteWarp(p.SteamID, warpName, true));
                            } else {

                                p.Message(DeleteWarp(p.SteamID, warpName, false));
                            }
                        } else {
                            p.Message("[FF2A55]/[-][FF7F2A]warp delete <warpname>[-]");
                           
                        }
                       

                        return true;
                    case "edit":
                        if (args.Length >= 3) {
                            warpName = args[2].ToLower();
                            string groupName = "all";
                            string updateCords = "no";
                            string enabled = "yes";
                            if (args.Length == 4) {
                                groupName = args[3].ToLower();
                                if (groupName != "all" && groupName != "private") {
                                    p.Message(bb._warningsC+"Invalid group name '"+groupName+"' only 'private' (you and friends) or 'all' (public warp)");
                                    return false;
                                }
                            }
                            if (args.Length == 5) {
                                updateCords = args[4].ToLower();
                                if (updateCords != "yes" && updateCords != "no") {
                                    p.Message(bb._warningsC + "Invalid updateCord argument: '" + updateCords + "' only 'yes' or 'no'");
                                    return false;
                                }
                            }
                            if (args.Length == 6) {
                                updateCords = args[5].ToLower();
                                if (enabled != "yes" && enabled != "no") {
                                    p.Message(bb._warningsC + "Invalid enabled argument: '" + enabled + "' only 'yes' or 'no'");
                                    return false;
                                }
                            }
                                server.Execute("lp");
                                Thread t = new Thread(() => {
                                    Thread.Sleep(800); //1 second delay
                                    int x = (int)p.CurrentPosition.X;
                                    int y = (int)p.CurrentPosition.Y;
                                    int z = (int)p.CurrentPosition.Z;
                                    p.Message(EditWarp(p.SteamID, warpName, x, y, z, p.IsAdmin ? true : false, groupName, updateCords, enabled));
                                });
                                t.IsBackground = true;
                                t.Start();
                        } else {
                            p.Message("[FF2A55]/[-][FF7F2A]warp edit <warpname> <group=private|all> <update cords=yes|no> <enable=yes|no>[-]");
                            p.Message(bb._cmdC + "Example: [-][FF2A55]/[-][FF7F2A]warp edit craptown all yes yes[-]");
                        }
                        
                        return true;
                    case "list":
                        warpsFound = 0;
                        string[] listwarps = ListWarps(p.SteamID, p.Name, false, 1, out warpsFound);
                        //if (warpsFound > 0) {
                            foreach (string print in listwarps) {
                                //m_Console.SendResult(print);
                                p.Message(print);
                            }                     
                       // }

                        return true;
                    case "unknown":
                        break;
                }


                warpName = args[1];
                int[] xyz = Load(p.SteamID, p.Name, out warpFound, warpName);
                if (warpFound == 2) {
                    kz_cmdTeleport.telePort(xyz[0], xyz[1], xyz[2], warpName, p.Name, p, server);
                }else if(warpFound == 1){
                    p.Message("[26C9FF] Warp: [-][FFFFFF]'" + args[1] + "' is a private warp");
                } else {
                    p.Message("[26C9FF] Warp: [-][FFFFFF]'" + args[1] + "' not found, did you spell it exactly? check /warps" + bbclose);
                }




            }
            return false;
        }

        public static void save_warpsXML() {
            string warps = "warps/";
            warps = "";
            try {
                _WarpsV2 saveObj = _WarpsV2.Instance;
                //Directory.CreateDirectory(GamePrefs.GetString(EnumGamePrefs.SaveGameFolder) + "/playerHomes");
                XmlSerializer serializer = new XmlSerializer(typeof(_WarpsV2));
                TextWriter textWriter = new StreamWriter(warps + "warps" + ".xml");
                serializer.Serialize(textWriter, saveObj);
                textWriter.Close();
            } catch (Exception e) {
                logger.Debug("Error in warps.save_warpsXML: " + e);
            }
        }

        public static void load__WarpsV2XML() {
            string warps = "warps/";
            warps = "";
            if (File.Exists(warps + "warps" + ".xml")) {
                try {
                    XmlSerializer deserializer = new XmlSerializer(typeof(_WarpsV2));
                    TextReader textReader = new StreamReader(warps + "warps" + ".xml");

                    _WarpsV2.Instance = (_WarpsV2)deserializer.Deserialize(textReader);

                    textReader.Close();
                    // return true;
                } catch (Exception e) {
                    logger.Debug("Error in _WarpsV2.loadXML: " + e);
                    _WarpsV2.Instance = new _WarpsV2();
                    //return true;
                }
            } else {
                logger.Debug("Error in _WarpsV2.loadXML: File Not Found");
                _WarpsV2.Instance = new _WarpsV2();
                // return true;
            }
        }

        public static int[] Load(string steamid, string playerName, out int warpFound, string warpName = "default") {

            int[] warp_xyz = { 0, 100, 0 };
            try {
                foreach (var itemNum in _WarpsV2.Instance.Profiles) {
                        foreach (var warpID in itemNum.WarpEntry) {
                            //logger.Debug(homeID.name.ToLower() +"==?"+ homeName.ToLower());
                            if (warpID.name.ToLower() == warpName.ToLower()) {
                                if (warpID.group == "all") {
                                    warp_xyz[0] = warpID.x;
                                    warp_xyz[1] = warpID.y;
                                    warp_xyz[2] = warpID.z;
                                    warpFound = 2;
                                    return warp_xyz;
                                } else if (itemNum.SteamID == steamid) {
                                    warp_xyz[0] = warpID.x;
                                    warp_xyz[1] = warpID.y;
                                    warp_xyz[2] = warpID.z;
                                    warpFound = 2;
                                    return warp_xyz;
                                } else {
                                    warp_xyz[0] = warpID.x;
                                    warp_xyz[1] = warpID.y;
                                    warp_xyz[2] = warpID.z;
                                    warpFound = 1;
                                    return warp_xyz;
                                }
                            }
                        }
                }
                //return home_xyz;

            } catch (Exception e) {
                logger.Debug("Error in _WarpsV2.Load: No warp found " + e);
            }
            warpFound = 0;
            return warp_xyz;
        }

        public static string[] ListWarps(string steamid, string playerName, bool publicList, int warpOutput, out int numFoundWarps) {

            numFoundWarps = 0;
            List<string> publicWarps = new List<string>();
            List<string> YourPrivateWarps = new List<string>();
            List<string> YourPublicWarps = new List<string>();

            if (!ReferenceEquals(_WarpsV2.Instance.Profiles, null)) {
                try {
                    foreach (var itemNum in _WarpsV2.Instance.Profiles) {
                        if (!publicList && itemNum.SteamID == steamid) {
                          //  numFoundWarps = itemNum.;
                            foreach (var warp in itemNum.WarpEntry) {
                               /* if (warp.group == "all") {
                                    YourPublicWarps.Add("[FF8000]Your public warp: '[-][FFFFFF]" + warp.name + bb._cc + "[FF8000]' - [-][FF8000]x[-][FFFFFF]: " + warp.x + bb._cc + " [FF8000]y[-][FFFFFF]: " + warp.y + bb._cc + " [FF8000]z[-][FFFFFF]: " + warp.z + bb._cc);
                                    numFoundWarps++;
                                }*/
                                if (warp.group == "private") {
                                    YourPrivateWarps.Add("[A76FDF]Your private warp: '[-][FFFFFF]" + warp.name + bb._cc + "[FF8000]' - [-][FF8000]x[-][FFFFFF]: " + warp.x + bb._cc + " [FF8000]y[-][FFFFFF]: " + warp.y + bb._cc + " [FF8000]z[-][FFFFFF]: " + warp.z + bb._cc);
                                    numFoundWarps++;
                                }
                            }
                        }
       
                        List<string> warps = new List<string>();

                        foreach (var warp in itemNum.WarpEntry) {
                            if (warp.group == "all") {
                               // publicWarps.Add("[F77225]Warp name: '[-][FFFFFF]" + warp.name + bb._cc + "[FF8000]' - [-][FF8000]x[-][FFFFFF]: " + warp.x + bb._cc + " [FF8000]y[-][FFFFFF]: " + warp.y + bb._cc + " [FF8000]z[-][FFFFFF]: " + warp.z + bb._cc + "[F77225] //Set by [-][FFFFFF]" + itemNum.PlayerName + bb._cc+"");
                                
                                if(warpOutput !=0){
                                    publicWarps.Add("[F77225]Warp name: '[-][FFFFFF]" + warp.name + bb._cc + "[F77225] //Set by [-][FFFFFF]" + itemNum.PlayerName + bb._cc + "");
                                }
                                warps.Add(warp.name);
                                numFoundWarps++;
                            }
                        }
                        if (numFoundWarps >0 && warpOutput == 0) {
                            

                            StringBuilder warpMessage = new StringBuilder();
                            //warpMessage.Append("[26C9FF] Use /warp list: [-] ");
                            int i = 0;
                            int length = warps.Count -1;
                            foreach (string warp in warps) {
                                if (i < 4) {
                                    if (i == 0) {
                                        warpMessage.Append("[00B9F2]Public warp names:[-] " + warp + " [-]");
                                    }else{
                                        warpMessage.Append("[00B9F2]| [-][FFFFFF] " + warp + " [-]");
                                    }
                                }
                                //                  
                                if (i >= 4 && (i % 4) == 0) {
                                    warpMessage.Append(" [00B9F2]| [-]");
                                    publicWarps.Add(warpMessage.ToString());
                                    warpMessage.Clear();
                                }

                                if (i > 3) {
                                    warpMessage.Append("[00B9F2]| [-][FFFFFF] " + warp + " [-]");
                                }
                                if (i >= 4 && length == i) {
                                    warpMessage.Append(" [00B9F2]|[-]");
                                }

                                i++;
                            }
                            publicWarps.Add(warpMessage.ToString());
                        }
                    }
                    if (numFoundWarps >0) {
                        YourPublicWarps.AddRange(YourPrivateWarps);
                        publicWarps.AddRange(YourPublicWarps);
                        string[] ret = publicWarps.ToArray();
                        return ret;
                    }

                } catch (Exception e) {
                    // Log.Out("Error in ListHomes.Load: No home found " + e);
                }
            }
            string[] retNone = { "[FF2A55]No warps found.[-]" };
            return retNone;

        }

        public static string EditWarp(string steamid, string warpName,int x, int y, int z,  bool isAdmin, string group, string updateCords = "no", string enabled = "yes") {

            if (!ReferenceEquals(_WarpsV2.Instance.Profiles, null)) {
                try {
                    foreach (var itemNum in _WarpsV2.Instance.Profiles) {

                        if (itemNum.SteamID == steamid || isAdmin) {
                            foreach (var Warp in itemNum.WarpEntry) {

                                if (Warp.name == warpName) {
                                    if (updateCords == "yes") {
                                        Warp.x = x;
                                        Warp.y = y;
                                        Warp.z = z;
                                    } else {
                                        Warp.x = Warp.x;
                                        Warp.y = Warp.y;
                                        Warp.z = Warp.z;
                                    }
                                    if (enabled == "yes") {
                                        Warp.enabled = true;
                                    } else if (enabled == "no") {
                                        Warp.enabled = false;
                                    }
                                    if (group == "all" || group == "private") {
                                        Warp.group = group;
                                    }
                                    Commands.kz_cmdWarpsV2.save_warpsXML();
                                    return bb._txt3C+"Warp '"+bb._cc+bb._white+warpName+bb._txt3C+"' updated."+bb._cc;
                                }
                            }
                            
                        }
                    }
                } catch (Exception e) {
                    //  Log.Out("Error in DelHome.Load: No home found " + e);
                }
            }
            return "No such warp '"+warpName+"' found, use exact warp name, check '/warp list' to make sure you can edit that warp.";
        }

        public static string DeleteWarp(string steamid, string warpName, bool isAdmin) {
            int warpIdNum = 0;
            int warpIdToDelete = -1;
            int profileID = 0;
            Commands.kz_cmdWarpsV2.load__WarpsV2XML();

            List<int> warpsToKeep = new List<int>();
            if (!ReferenceEquals(_WarpsV2.Instance.Profiles, null)) {
                try {
                    int profileCounter = 0;
                    foreach (var itemNum in _WarpsV2.Instance.Profiles) {

                        if (itemNum.SteamID == steamid || isAdmin) {
                            profileID = profileCounter;
                            foreach (var Warp in itemNum.WarpEntry) {

                                if (Warp.name == warpName) {
                                    warpIdToDelete = warpIdNum;
                                } else {
                                    warpsToKeep.Add(warpIdNum);
                                }
                                warpIdNum += 1;
                            }
                            if (warpIdToDelete != -1) {
                                //HomesProfileHome[] tempHomes = new HomesProfileHome[newKey];
                                int currentWarps = warpsToKeep.Count;

                                WarpEntry[] tempWarps = new WarpEntry[currentWarps];
                                int key = 0;
                                foreach (int i in warpsToKeep) {
                                    tempWarps[key] = new WarpEntry();
                                    tempWarps[key].name = _WarpsV2.Instance.Profiles[profileID].WarpEntry[i].name;
                                    tempWarps[key].group = _WarpsV2.Instance.Profiles[profileID].WarpEntry[i].group;
                                    tempWarps[key].enabled = _WarpsV2.Instance.Profiles[profileID].WarpEntry[i].enabled;
                                    tempWarps[key].x = _WarpsV2.Instance.Profiles[profileID].WarpEntry[i].x;
                                    tempWarps[key].y = _WarpsV2.Instance.Profiles[profileID].WarpEntry[i].y;
                                    tempWarps[key].z = _WarpsV2.Instance.Profiles[profileID].WarpEntry[i].z;
                                    key += 1;
                                }

                                _WarpsV2.Instance.Profiles[profileID].WarpEntry = new WarpEntry[currentWarps];
                                tempWarps.CopyTo(_WarpsV2.Instance.Profiles[profileID].WarpEntry, 0);
                               // int maxhome = _WarpsV2.Instance.Profiles[profileID].maxHomes;
                                Commands.kz_cmdWarpsV2.save_warpsXML();
                                return "Warp deleted: "+warpName;
                                //return "homes left: " + bb._itemC + currentWarps + bb._txtC + "/" + bb._itemC + maxhome; //return number of extra homes
                            }

                        }
                        profileCounter += 1;
                    }
                } catch (Exception e) {
                      Console.WriteLine("Error in warp.delete: " + e);
                }
            }

            if (isAdmin) {
                return "Warp not found to delete.";
            } else {
                return "No warp with that name that you can delete.";
            }
        }

        public static void Save(IPlayer p, string steamid, string playerName, int x, int y, int z, string warpName, string group, bool enabled) {

            string message = "";
            bool warpSet = false;
            _warpName = warpName;

            bool matched = false;
            foreach (string value in argsReserved) {
                if (String.Equals(value, _warpName, StringComparison.OrdinalIgnoreCase) == true) {
                    matched = true;
                    message = bb._warningsC + "The warp " + bb._cc + bb._white + _warpName + bb._cc + bb._warningsC + " is a reserved name that cannot be used for warp names." + bb._cc;

                        p.Message(message);
                    
                    return;
                }
            }
            Commands.kz_cmdWarpsV2.load__WarpsV2XML();

         
            if (!matched) {
                bool foundprofile = false;
                //maxHomes = 0;
                int numFoundWarps = 0;

                bool itsNull = false;
                if (!ReferenceEquals(_WarpsV2.Instance.Profiles, null)) {
                    try {
                        foreach (var itemNum in _WarpsV2.Instance.Profiles) {
                            foreach (var warpID in itemNum.WarpEntry) {
                                if (warpID.name == warpName && group == warpID.group) {

                                p.Message(bb._warningsC + "Warp name: " + bb._cc + bb._white + warpName + bb._cc + bb._warningsC + " (created by " + bb._white + itemNum.PlayerName + bb._cc + bb._warningsC + ") is already used, please choose another warp name." + bb._cc);
                                    return;
                                }
                            }
                        
                        }                   
                        foreach (var itemNum in _WarpsV2.Instance.Profiles) {
                            if (itemNum.SteamID == steamid) {
                                foundprofile = true;
                                foreach (var warpID in itemNum.WarpEntry) {
                                    numFoundWarps += 1;
                                    if (warpID.name == _warpName) {
                                        warpID.x = x;
                                        warpID.y = y;
                                        warpID.z = z;
                                        warpID.enabled = enabled;
                                        warpID.group = group;
                                        warpSet = true;
                                        return;
                                    }
                                }

                                // int num = itemNum.Home.Length; //using foundprofilehomes
                                // if (itemNum.maxHomes > numFoundHomes)
                                // {
                                //First home always has to be default
                                /*
                                if (numFoundWarps == 0)
                                    {
                                        if (_warpName != "default")
                                        {
                                            _warpName = "default";
                                        }
                                    }
                                */
                                WarpEntry[] newWarp = new WarpEntry[numFoundWarps + 1];
                                newWarp[numFoundWarps] = new WarpEntry();
                                newWarp[numFoundWarps].name = _warpName;
                                newWarp[numFoundWarps].x = x;
                                newWarp[numFoundWarps].y = y;
                                newWarp[numFoundWarps].z = z;
                                newWarp[numFoundWarps].enabled = enabled;
                                newWarp[numFoundWarps].group = group;

                                itemNum.WarpEntry.CopyTo(newWarp, 0);

                                itemNum.WarpEntry = new WarpEntry[numFoundWarps + 1];
                                newWarp.CopyTo(itemNum.WarpEntry, 0);

                                //string test = itemNum.Home[1].name;
                                warpSet = true;
                                if (warpSet) {
                                    p.Message(bb._txtC + "Public warp set, use '[-]" + bb._cmdC + "/warp [-]" + bb._argC + warpName + bb._cc + bb._txtC + "' to warp to here again.[-]");
                                    save_warpsXML();
                                }
                                return;
                                /*
                            }
                            else
                            {
                                // m_Console.SendResult("Your maxhomes(" + itemNum.maxHomes.ToString() + ") has been reached (ask Koolio for more) or use \"sethome <existing homename>\" or delete an existing home name \"delhome <name>\" before making a new one with a different name.");
                                p.Message(alertC + "Your maxhomes " + bbclose + bb.bb._argC + itemNum.maxHomes.ToString() + bbclose + alertC + " has been reached (ask Koolio for more) or use '" + bbclose + bb._cmdC + "/sethome <existing homename>" + bbclose + alertC + "' or delete an existing home name '" + bbclose + bb._cmdC + "/delhome <homename>" + bbclose + alertC + "' before making a new one with another name." + bbclose, servername, _sender);
                                warpSet = false;
                                return;
                            }
                                */
                            }
                        }

                    } catch (Exception e) {
                        logger.Debug("Error in _WarpsV2.Save: " + e);

                    }
                } else {
                    Console.WriteLine("_WarpsV2.Instance.Profiles is null");
                    itsNull = true;
                }
                if (foundprofile == false) {
                    try {
                        //First home always has to be default
                        if (numFoundWarps == 0) {
                            /*
                            if (_warpName != "default")
                            {
                                _warpName = "default";
                            }*/
                        }
                        Warp newProfile = new Warp();
                        newProfile.PlayerName = playerName;
                        newProfile.SteamID = steamid;
                        //newProfile.maxHomes = 1;

                        WarpEntry newWarp = new WarpEntry();
                        newWarp.name = _warpName;
                        newWarp.x = x;
                        newWarp.y = y;
                        newWarp.z = z;
                        newWarp.enabled = enabled;
                        newWarp.group = group;

                        newProfile.WarpEntry = new WarpEntry[1];
                        newProfile.WarpEntry[0] = newWarp;

                        int num = 0;
                        if (!itsNull) {
                            num = _WarpsV2.Instance.Profiles.Length;
                            Warp[] tempWarpProfiles = new Warp[num + 1];
                            _WarpsV2.Instance.Profiles.CopyTo(tempWarpProfiles, 0);
                            //tempHomeProfiles[num+1] causes null..  very weird
                            tempWarpProfiles[num] = newProfile;
                            _WarpsV2.Instance = new _WarpsV2();
                            _WarpsV2.Instance.Profiles = new Warp[num + 1];
                            tempWarpProfiles.CopyTo(_WarpsV2.Instance.Profiles, 0);
                        } else {
                            Warp[] tempWarpProfiles = new Warp[num + 1];
                            tempWarpProfiles[num] = newProfile;
                            _WarpsV2.Instance = new _WarpsV2();
                            _WarpsV2.Instance.Profiles = new Warp[num + 1];
                            tempWarpProfiles.CopyTo(_WarpsV2.Instance.Profiles, 0);
                        }
                        warpSet = true;
                        // logger.Debug(Homes.Instance.Profiles[0].PlayerName);
                        if (warpSet) {
                            p.Message(bb._txtC + "Public warp set, use '[-]" + bb._cmdC + "/warp [-]" + bb._argC + warpName + bb._cc + bb._txtC + "' to warp to here again.[-]");
                            save_warpsXML();
                        }

                    } catch (Exception e) {
                        logger.Debug("3Error in _WarpsV2.Save else" + e);

                    }
                }
            }
        }


        /// <remarks/>
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        [System.Xml.Serialization.XmlRootAttribute(Namespace = "", IsNullable = false)]
        public class _WarpsV2 {

            private static _WarpsV2 instance;
            public static _WarpsV2 Instance {
                get {
                    if (instance == null) {
                        instance = new _WarpsV2();
                    }
                    return instance;
                }
                set {
                    instance = value;
                }
            }


            private Warp[] warpsField;

            /// <remarks/>
            [System.Xml.Serialization.XmlArrayItemAttribute("Warps", IsNullable = false)]
            public Warp[] Profiles {
                get {
                    return this.warpsField;
                }
                set {
                    this.warpsField = value;
                }
            }
        }
        /// <remarks/>
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class Warp {

            private WarpEntry[] warpField;

            private string playerNameField;

            private string steamIDField;

            /// <remarks/>
            [System.Xml.Serialization.XmlElementAttribute("Warp")]
            public WarpEntry[] WarpEntry {
                get {
                    return this.warpField;
                }
                set {
                    this.warpField = value;
                }
            }

            /// <remarks/>
            [System.Xml.Serialization.XmlAttributeAttribute()]
            public string PlayerName {
                get {
                    return this.playerNameField;
                }
                set {
                    this.playerNameField = value;
                }
            }

            /// <remarks/>
            [System.Xml.Serialization.XmlAttributeAttribute()]
            public string SteamID {
                get {
                    return this.steamIDField;
                }
                set {
                    this.steamIDField = value;
                }
            }

        }

        /// <remarks/>
        [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
        public partial class WarpEntry {
            private string nameField;

            private string groupField;

            private bool enabledField;

            private int xField;

            private int yField;

            private int zField;

            /// <remarks/>
            [System.Xml.Serialization.XmlAttributeAttribute()]
            public string name {
                get {
                    return this.nameField;
                }
                set {
                    this.nameField = value;
                }
            }
            /// <remarks/>
            [System.Xml.Serialization.XmlAttributeAttribute()]
            public string group {
                get {
                    return this.groupField;
                }
                set {
                    this.groupField = value;
                }
            }
            /// <remarks/>
            [System.Xml.Serialization.XmlAttributeAttribute()]
            public bool enabled {
                get {
                    return this.enabledField;
                }
                set {
                    this.enabledField = value;
                }
            }
            /// <remarks/>
            [System.Xml.Serialization.XmlAttributeAttribute()]
            public int x {
                get {
                    return this.xField;
                }
                set {
                    this.xField = value;
                }
            }

            /// <remarks/>
            [System.Xml.Serialization.XmlAttributeAttribute()]
            public int y {
                get {
                    return this.yField;
                }
                set {
                    this.yField = value;
                }
            }

            /// <remarks/>
            [System.Xml.Serialization.XmlAttributeAttribute()]
            public int z {
                get {
                    return this.zField;
                }
                set {
                    this.zField = value;
                }
            }
        }
    }
}

