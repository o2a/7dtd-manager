﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace _7DTDManager
{
    public class _bbcode
    {

        public static string _servername = "[FFFFFF]ᴷᶻ[-]";
        public static string _servernameAll = "[FFFFFF]ᴷᶻ[-][7F7FFF]»[-]";
        public static string _fsC = "[07D907]";
        //forwardslash
        public static string _cmdC = "[7FFF55]";
        public static string _argC = "[7FFF55]";
        public static string _txtC = "[FFFFFF]";
        public static string _txt2C = "[AABCCF]";
        public static string _txt3C = "[7999BB]";
        public static string _linkC = "[FF5500]";
        public static string _alertC = "[FF5500]";
        public static string _warningsC = "[F99A3B]";
        public static string _errorsC = "[F50243]";
        public static string _cordsC = "[FF8000]";
        public static string _itemC = "[FFFFFF]";


        public static string _white = "[FFFFFF]";
        public static string _yellow = "[FFFF00]";
        public static string _orange = "[FF552A]";
        public static string _lightOrange = "[FF7F2A]";
        public static string _purple = "[AA00AA]";
        public static string _lightGreen = "[7FFF55]";

        public static string _lightBlue = "[7FAAFF]";

        public static string _close = "[-]";
        public static string _cc = "[-]";

        
        public string servername
        {
            get { return _servername; }
            set { _servername = value; }
        }
        public string servernameAll
        {
            get { return _servernameAll; }
            set { _servernameAll = value; }
        }
        public string fsC
        {
            get { return _fsC; }
            set { _fsC = value; }
        }
        public string cmdC
        {
            get { return _cmdC; }
            set { _cmdC = value; }
        }
        public string argC
        {
            get { return _argC; }
            set { _argC = value; }
        }
        public string txtC
        {
            get { return _txtC; }
            set { _txtC = value; }
        }
        public string txt2C
        {
            get { return _txt2C; }
            set { _txt2C = value; }
        }

        public string txt3C
        {
            get { return _txt3C; }
            set { _txt3C = value; }
        }
        public string linkC
        {
            get { return _linkC; }
            set { _linkC = value; }
        }
        public string alertC
        {
            get { return _alertC; }
            set { _alertC = value; }
        }

        public string warningsC
        {
            get { return _warningsC; }
            set { _warningsC = value; }
        }
        public string errorsC
        {
            get { return _errorsC; }
            set { _errorsC = value; }
        }
        public string cordsC
        {
            get { return _cordsC; }
            set { _cordsC = value; }
        }
        public string itemC
        {
            get { return _itemC; }
            set { _itemC = value; }
        }
        public string white
        {
            get { return _white; }
            set { _white = value; }
        }
        public string yellow
        {
            get { return _yellow; }
            set { _yellow = value; }
        }
        public string orange
        {
            get { return _orange; }
            set { _orange = value; }
        }
        public string lightOrange
        {
            get { return _lightOrange; }
            set { _lightOrange = value; }
        }

        public string purple
        {
            get { return _purple; }
            set { _purple = value; }
        }
        public string lightGreen
        {
            get { return _lightGreen; }
            set { _lightGreen = value; }
        }

        public string lightBlue
        {
            get { return _lightBlue; }
            set { _lightBlue = value; }
        }
        public string cc
        {
            get { return _cc;}
            set { _cc = value; }
        }

    }
}
