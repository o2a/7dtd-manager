﻿using MySql.Data.MySqlClient;
using NLog;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _7DTDManager {

    class _Log_chat {
        private static Logger logger = LogManager.GetCurrentClassLogger();

        public static void Update_chat(string SteamID, string PlayerName, string data, string type, int x, int y, int z) {
            //Console.WriteLine("Adding _Log_chat: " + SteamID + " // " + PlayerName + " " + data + " // " + type + "x" + x + "y" + y + "z" + z);

            /*
            using (System.IO.StreamWriter file = new System.IO.StreamWriter("log_chat.txt")) {
                file.WriteLine("Adding _Log_chat: " + SteamID + " // " + PlayerName + " // " + data + " " + type + "x" + x + "y" + y + "z" + z);
            }
             */
            try {
                using (var con = new MySqlConnection(_Mysql.localDB)) {
                    using (var cmd = con.CreateCommand()) {
                        con.Open();

                        string mysql_insert = @"INSERT INTO log_chat(
                                         SteamID
                                         ,Name
                                         ,Action
                                         ,Data
                                         ,timeStamp
                                         ,x
                                         ,y
                                         ,z
                                        )
                                        VALUES
                                        (
                                         @SteamID
                                         ,@Name
                                         ,@Action
                                         ,@Data
                                         ,@timeStamp
                                         ,@x
                                         ,@y
                                         ,@z
                                        );";


                        MySqlCommand mypcmd = new MySqlCommand(mysql_insert, con);
                        mypcmd.Prepare();
                        mypcmd.Parameters.AddWithValue("@SteamID", SteamID);
                        mypcmd.Parameters.AddWithValue("@Name", PlayerName);
                        mypcmd.Parameters.AddWithValue("@Action", type);
                        mypcmd.Parameters.AddWithValue("@Data", data);
                        mypcmd.Parameters.AddWithValue("@timeStamp", DateTime.Now);
                        mypcmd.Parameters.AddWithValue("@x", x);
                        mypcmd.Parameters.AddWithValue("@y", y);
                        mypcmd.Parameters.AddWithValue("@z", z);
                        mypcmd.ExecuteNonQuery();

                        cmd.Dispose();
                    }
                    // clean up
                    
                    con.Close();
                }
            } catch (Exception e) {
                Console.WriteLine("Error _Log_chat: " + e.Message);
                using (System.IO.StreamWriter file = new System.IO.StreamWriter("errors.txt",true)) {
                    file.WriteLine("Error _Log_chat: " + e.Message);
                }
                logger.Debug("Error _Log_chat: " + e.Message);
            }

        }
    }
}